USE Finder;
--GO

--CREATE SCHEMA FinderSchema AUTHORIZATION dbo
--go

IF NOT EXISTS(SELECT * FROM sys.fulltext_catalogs WHERE name = 'FinderCatalog')
BEGIN
	CREATE FULLTEXT CATALOG FinderCatalog AS DEFAULT;
END

IF OBJECT_ID('FinderSchema.Account', 'U') IS NOT NULL
	DROP TABLE FinderSchema.Account;

IF OBJECT_ID('FinderSchema.Preference', 'U') IS NOT NULL
	DROP TABLE FinderSchema.Preference;

IF OBJECT_ID('FinderSchema.ArticleAuthor', 'U') IS NOT NULL
	DROP TABLE FinderSchema.ArticleAuthor;

IF OBJECT_ID('FinderSchema.ArticleKeyword', 'U') IS NOT NULL
	DROP TABLE FinderSchema.ArticleKeyword;

IF OBJECT_ID('FinderSchema.Author', 'U') IS NOT NULL
	DROP TABLE FinderSchema.Author;

IF OBJECT_ID('FinderSchema.Article', 'U') IS NOT NULL
	DROP TABLE FinderSchema.Article;

IF OBJECT_ID('FinderSchema.Magazine', 'U') IS NOT NULL
	DROP TABLE FinderSchema.Magazine;

IF OBJECT_ID('FinderSchema.Keyword', 'U') IS NOT NULL
	DROP TABLE FinderSchema.Keyword;

PRINT N'--- INSTALLING SYSTEM PREFERENCES ---';
CREATE TABLE FinderSchema.Account (
	AccountID BIGINT IDENTITY(1, 1) NOT NULL,
	AccountUser VARCHAR(100) NOT NULL,
	AccountPassword VARBINARY(256) NOT NULL,
	AccountSalt VARBINARY(256) NOT NULL,
	AccountRoles VARCHAR(100),
	AccountEmail VARCHAR(200),
	UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT AccountPK PRIMARY KEY (AccountID)
);

CREATE TABLE FinderSchema.Preference (
	PrefID BIGINT IDENTITY(1, 1) NOT NULL,
	PrefName VARCHAR(500) NOT NULL,
	PrefValue VARCHAR(500) NOT NULL,
	UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT PreferencePK PRIMARY KEY (PrefID),
	CONSTRAINT UniquePrefName UNIQUE (PrefName)
);

PRINT N'--- DONE! ---';

CREATE TABLE FinderSchema.Magazine (
	MagazineID BIGINT NOT NULL IDENTITY(1, 1),
	MagazineNumber BIGINT NOT NULL,
	MagazineName VARCHAR(MAX) NOT NULL DEFAULT '',
	PublishYear INT NOT NULL,
	UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT MagazinePK PRIMARY KEY (MagazineID)
);

CREATE UNIQUE INDEX UniqueMagazine ON FinderSchema.Magazine(MagazineNumber, PublishYear);

CREATE TABLE FinderSchema.Article (
	ArticleID BIGINT NOT NULL IDENTITY(1, 1),
	ArticleNumber INT NOT NULL,
	ArticleTitle VARCHAR(MAX) NOT NULL DEFAULT '',
	ArticleFile VARCHAR(500) NOT NULL,
	ArticleDesc VARCHAR(MAX) NOT NULL DEFAULT '',
	ArticlePageCount INT NOT NULL DEFAULT 0,
	UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	MagazineID BIGINT NOT NULL,
	CONSTRAINT ArticlePK PRIMARY KEY (ArticleID),
	CONSTRAINT UniqueArticle UNIQUE (ArticleFile),
	CONSTRAINT FKArticleRefMagazine
		FOREIGN KEY (MagazineID) REFERENCES FinderSchema.Magazine (MagazineID)
			ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE FULLTEXT INDEX ON FinderSchema.Article(ArticleTitle, ArticleDesc)
	KEY INDEX ArticlePK
	WITH STOPLIST = SYSTEM, CHANGE_TRACKING = AUTO;

CREATE TABLE FinderSchema.Author (
	AuthorID BIGINT NOT NULL IDENTITY(1, 1),
	AuthorName VARCHAR(200) COLLATE Latin1_General_CI_AI,
	AuthorFName VARCHAR(500) COLLATE Latin1_General_CI_AI,
	AuthorMName VARCHAR(200) COLLATE Latin1_General_CI_AI,
	UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT AuthorPK PRIMARY KEY (AuthorID),
	CONSTRAINT UQAuthor UNIQUE CLUSTERED (AuthorFName, AuthorMName, AuthorName)
);

CREATE FULLTEXT INDEX ON FinderSchema.Author (AuthorFName, AuthorMName, AuthorName)
	KEY INDEX AuthorPK
	WITH STOPLIST = SYSTEM, CHANGE_TRACKING = AUTO;

CREATE TABLE FinderSchema.ArticleAuthor (
	ArticleID BIGINT NOT NULL,
	AuthorID BIGINT NOT NULL,
	UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT ArtAuthAuthorFK
		FOREIGN KEY (AuthorID) REFERENCES FinderSchema.Author(AuthorID)
		ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT ArtAuthArticleFK
		FOREIGN KEY (ArticleID) REFERENCES FinderSchema.Article(ArticleID)
		ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT ArticleAuthorPK PRIMARY KEY (ArticleID, AuthorID),
	CONSTRAINT UniqueArticleAuthor UNIQUE (ArticleID, AuthorID)
);

CREATE TABLE FinderSchema.Keyword (
	KeywordID BIGINT IDENTITY(1, 1),
	KeywordValue VARCHAR(500),
	UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT KeywordPK PRIMARY KEY (KeywordID),
	CONSTRAINT UQKeyword UNIQUE (KeywordValue)
);

CREATE FULLTEXT INDEX ON FinderSchema.Keyword (KeywordValue)
	KEY INDEX KeywordPK
	WITH STOPLIST = SYSTEM, CHANGE_TRACKING = AUTO;

CREATE TABLE FinderSchema.ArticleKeyword (
	ArticleID BIGINT NOT NULL,
	KeywordID BIGINT NOT NULL,
	UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	CONSTRAINT ArtKeyArticleFK
		FOREIGN KEY (ArticleID) REFERENCES FinderSchema.Article(ArticleID)
		ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT ArtKeyKeywordFK
		FOREIGN KEY (KeywordID) REFERENCES FinderSchema.Keyword(KeywordID)
		ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT ArticleKeywordPK PRIMARY KEY (ArticleID, KeywordID),
	CONSTRAINT UniqueArticleKeyword UNIQUE (ArticleID, KeywordID)
);

PRINT N'--- Creating user Root ---';
INSERT INTO FinderSchema.Account
	(AccountUser, AccountPassword, AccountSalt, AccountRoles)
	VALUES
	('Root', 0x49923E3273BEB93952D0BC898FDE06D4244FB0D2A53DA5E5645E2124867EAE4851DF3E566C72B2601EA2C5F346C6B5B2E4AE29C31274598E6D7C398EACEACCD8, 0x226B1D4C43DDE909F5F4611FB7CECFA618C3444B2471701A274C026E548BA9CB520A96B7BC6DC1A4F26E8EACBC19DC381537, 'Admin');

INSERT INTO FinderSchema.Preference (PrefName, PrefValue) VALUES 
('results_per_page', '15'),
('pagination_range', '3');

PRINT N'DONE! ---';