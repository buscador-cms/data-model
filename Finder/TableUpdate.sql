USE Finder;

ALTER TABLE FinderSchema.Magazine
	ADD UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE FinderSchema.Author
	ADD UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE FinderSchema.ArticleAuthor
	ADD UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE FinderSchema.Article
	ADD UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE FinderSchema.Keyword
	ADD UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE FinderSchema.ArticleKeyword
	ADD UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE FinderSchema.Preference
	ADD UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE FinderSchema.Account
	ADD UpdateCheck DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;