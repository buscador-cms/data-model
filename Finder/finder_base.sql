USE [Finder]
GO
/****** Object:  Table [FinderSchema].[Account]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [FinderSchema].[Account](
	[AccountID] [bigint] IDENTITY(1,1) NOT NULL,
	[AccountUser] [varchar](100) NULL,
	[AccountPassword] [varbinary](256) NOT NULL,
	[AccountSalt] [varbinary](256) NOT NULL,
	[AccountRoles] [varchar](100) NULL,
	[AccountEmail] [varchar](100) NULL,
	[UpdateCheck] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [AccountPK] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [FinderSchema].[Article]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [FinderSchema].[Article](
	[ArticleID] [bigint] IDENTITY(1,1) NOT NULL,
	[ArticleNumber] [int] NOT NULL,
	[ArticleTitle] [varchar](max) NULL DEFAULT (''),
	[ArticleFile] [varchar](500) NULL,
	[ArticleDesc] [varchar](max) NULL DEFAULT (''),
	[ArticlePageCount] [int] NOT NULL DEFAULT ((0)),
	[UpdateCheck] [datetime] NOT NULL DEFAULT (getdate()),
	[MagazineID] [bigint] NOT NULL,
 CONSTRAINT [ArticlePK] PRIMARY KEY CLUSTERED 
(
	[ArticleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UniqueArticle] UNIQUE NONCLUSTERED 
(
	[ArticleFile] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [FinderSchema].[ArticleAuthor]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FinderSchema].[ArticleAuthor](
	[ArticleID] [bigint] NOT NULL,
	[AuthorID] [bigint] NOT NULL,
	[UpdateCheck] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [ArticleAuthorPK] PRIMARY KEY CLUSTERED 
(
	[ArticleID] ASC,
	[AuthorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UniqueArticleAuthor] UNIQUE NONCLUSTERED 
(
	[ArticleID] ASC,
	[AuthorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [FinderSchema].[ArticleKeyword]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [FinderSchema].[ArticleKeyword](
	[ArticleID] [bigint] NOT NULL,
	[KeywordID] [bigint] NOT NULL,
	[UpdateCheck] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [ArticleKeywordPK] PRIMARY KEY CLUSTERED 
(
	[ArticleID] ASC,
	[KeywordID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UniqueArticleKeyword] UNIQUE NONCLUSTERED 
(
	[ArticleID] ASC,
	[KeywordID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [FinderSchema].[Author]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [FinderSchema].[Author](
	[AuthorID] [bigint] IDENTITY(1,1) NOT NULL,
	[AuthorName] [varchar](200) NULL,
	[AuthorFName] [varchar](500) NULL,
	[AuthorMName] [varchar](200) NULL,
	[UpdateCheck] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [AuthorPK] PRIMARY KEY NONCLUSTERED 
(
	[AuthorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQAuthor] UNIQUE CLUSTERED 
(
	[AuthorFName] ASC,
	[AuthorMName] ASC,
	[AuthorName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [FinderSchema].[Keyword]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [FinderSchema].[Keyword](
	[KeywordID] [bigint] IDENTITY(1,1) NOT NULL,
	[KeywordValue] [varchar](500) NULL,
	[UpdateCheck] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [KeywordPK] PRIMARY KEY CLUSTERED 
(
	[KeywordID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQKeyword] UNIQUE NONCLUSTERED 
(
	[KeywordValue] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [FinderSchema].[Magazine]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [FinderSchema].[Magazine](
	[MagazineID] [bigint] IDENTITY(1,1) NOT NULL,
	[MagazineNumber] [bigint] NOT NULL,
	[MagazineName] [varchar](max) NULL DEFAULT (''),
	[PublishYear] [int] NOT NULL,
	[UpdateCheck] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [MagazinePK] PRIMARY KEY CLUSTERED 
(
	[MagazineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [FinderSchema].[Preference]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [FinderSchema].[Preference](
	[PrefID] [bigint] IDENTITY(1,1) NOT NULL,
	[PrefName] [varchar](500) NULL,
	[PrefValue] [varchar](500) NULL,
	[UpdateCheck] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [PreferencePK] PRIMARY KEY CLUSTERED 
(
	[PrefID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UniquePrefName] UNIQUE NONCLUSTERED 
(
	[PrefName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [FinderSchema].[ArticleAuthorView]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [FinderSchema].[ArticleAuthorView]
AS
	SELECT AuthorFName, AuthorMName, AuthorName, AA.ArticleID, AA.AuthorID 
	FROM FinderSchema.ArticleAuthor AA
		RIGHT OUTER JOIN FinderSchema.Author ON Author.AuthorID = AA.AuthorID
		LEFT OUTER JOIN FinderSchema.Article ON Article.ArticleID = AA.ArticleID

GO
/****** Object:  View [FinderSchema].[ArticleKeywordView]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [FinderSchema].[ArticleKeywordView]
AS
	SELECT KeywordValue, AK.ArticleID, AK.KeywordID
	FROM FinderSchema.ArticleKeyword AK
		RIGHT OUTER JOIN FinderSchema.Article ON Article.ArticleID = AK.ArticleID
		LEFT OUTER JOIN FinderSchema.Keyword ON Keyword.KeywordID = AK.KeywordID

GO
/****** Object:  View [FinderSchema].[ArticleView]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [FinderSchema].[ArticleView]
AS
	SELECT Article.*
	FROM FinderSchema.Article

GO
/****** Object:  View [FinderSchema].[AuthorView]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [FinderSchema].[AuthorView]
AS
	SELECT DISTINCT
		AuthorID, AuthorFName, AuthorMName, AuthorName,
		(AuthorFName + ' ' + AuthorMName + AuthorName) AS AuthorFullName
	FROM FinderSchema.Author

GO
/****** Object:  View [FinderSchema].[MagArtAutView]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [FinderSchema].[MagArtAutView]
AS
	SELECT DISTINCT
		A.ArticleID, ArticleNumber, ArticleTitle, ArticleFile, ArticleDesc, ArticlePageCount, A.MagazineID,
		MagazineName, MagazineNumber, PublishYear,
		CAST('Volumen ' + CAST(MagazineNumber AS VARCHAR) + ' del año ' + CAST(PublishYear AS VARCHAR) AS VARCHAR(MAX)) AS MagazineTitle,
		CAST(
			CAST(M.MagazineID AS VARCHAR) + ';' +
			CAST(MagazineNumber AS VARCHAR) + ';' +
			CAST(PublishYear AS VARCHAR)
		AS VARCHAR(MAX)) AS MagazineMeta,
		STUFF(
		(
			SELECT DISTINCT ',' + CAST(A.AuthorID AS VARCHAR) + ';' + A.AuthorFName + ' ' + A.AuthorMName + ' ' + A.AuthorName
			FROM FinderSchema.ArticleAuthor StuffAA
				INNER JOIN FinderSchema.Author A ON StuffAA.AuthorID = A.AuthorID
			WHERE StuffAA.ArticleID = AA.ArticleID
			FOR XML PATH(''), TYPE
		).value('.', 'varchar(max)'), 1, 1, ''
	) Authors
	FROM FinderSchema.Magazine M
		LEFT JOIN FinderSchema.Article A ON A.MagazineID = M.MagazineID
		LEFT JOIN FinderSchema.ArticleAuthor AA ON AA.ArticleID = A.ArticleID
		LEFT JOIN FinderSchema.Author Aut ON Aut.AuthorID = AA.AuthorID

GO
/****** Object:  View [FinderSchema].[MagazineArticleView]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [FinderSchema].[MagazineArticleView]
AS
	SELECT MagazineName, MagazineNumber, PublishYear, Article.*
	FROM FinderSchema.Magazine
		LEFT JOIN FinderSchema.Article ON Article.MagazineID = Magazine.MagazineID

GO
/****** Object:  View [FinderSchema].[MagazineView]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [FinderSchema].[MagazineView]
AS
	SELECT
		MagazineName, MagazineNumber, PublishYear,
		CAST('Volumen ' + CAST(MagazineNumber AS VARCHAR) + ' del año ' + CAST(PublishYear AS VARCHAR) AS VARCHAR(MAX)) AS MagazineTitle,
		CAST(
			CAST(M.MagazineID AS VARCHAR) + ';' +
			CAST(MagazineNumber AS VARCHAR) + ';' +
			CAST(PublishYear AS VARCHAR)
		AS VARCHAR(MAX)) AS MagazineMeta
	FROM FinderSchema.Magazine M

GO
/****** Object:  View [FinderSchema].[PreferenceView]    Script Date: 03-11-2016 19:32:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [FinderSchema].[PreferenceView]
AS
	SELECT * FROM FinderSchema.Preference;

GO
ALTER TABLE [FinderSchema].[Article]  WITH CHECK ADD  CONSTRAINT [FKArticleRefMagazine] FOREIGN KEY([MagazineID])
REFERENCES [FinderSchema].[Magazine] ([MagazineID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [FinderSchema].[Article] CHECK CONSTRAINT [FKArticleRefMagazine]
GO
ALTER TABLE [FinderSchema].[ArticleAuthor]  WITH CHECK ADD  CONSTRAINT [ArtAuthArticleFK] FOREIGN KEY([ArticleID])
REFERENCES [FinderSchema].[Article] ([ArticleID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [FinderSchema].[ArticleAuthor] CHECK CONSTRAINT [ArtAuthArticleFK]
GO
ALTER TABLE [FinderSchema].[ArticleAuthor]  WITH CHECK ADD  CONSTRAINT [ArtAuthAuthorFK] FOREIGN KEY([AuthorID])
REFERENCES [FinderSchema].[Author] ([AuthorID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [FinderSchema].[ArticleAuthor] CHECK CONSTRAINT [ArtAuthAuthorFK]
GO
ALTER TABLE [FinderSchema].[ArticleKeyword]  WITH CHECK ADD  CONSTRAINT [ArtKeyArticleFK] FOREIGN KEY([ArticleID])
REFERENCES [FinderSchema].[Article] ([ArticleID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [FinderSchema].[ArticleKeyword] CHECK CONSTRAINT [ArtKeyArticleFK]
GO
ALTER TABLE [FinderSchema].[ArticleKeyword]  WITH CHECK ADD  CONSTRAINT [ArtKeyKeywordFK] FOREIGN KEY([KeywordID])
REFERENCES [FinderSchema].[Keyword] ([KeywordID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [FinderSchema].[ArticleKeyword] CHECK CONSTRAINT [ArtKeyKeywordFK]
GO
