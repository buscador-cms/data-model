USE Finder;
DECLARE @ID BIGINT
DECLARE @AID BIGINT

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_2_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Yuri', @AuthorFName = 'Carvajal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Carvajal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Esteban', @AuthorFName = 'Figueroa', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Ibacache', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fabiola', @AuthorFName = 'Jaramillo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mónica', @AuthorFName = 'Winkler', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Francisca', @AuthorFName = 'Florenzano', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fabiola', @AuthorFName = 'Marín', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Josefa', @AuthorFName = 'Palacios', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'Garland', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pablo', @AuthorFName = 'Parés', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Naciones Unidas. Departamento de asuntos económicos y sociales. División de población: políticas referentes al aborto en el mundo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'año', @AuthorFName = '2007', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Patricio', @AuthorFName = 'Hevia', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Daniela', @AuthorFName = 'Araneda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_3_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Guido', @AuthorFName = 'Girardi', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mariano', @AuthorFName = 'Ruiz', @AuthorMName = 'Esquide', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ricardo', @AuthorFName = 'Muza', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Manuel', @AuthorFName = 'Ipinza', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Felipe', @AuthorFName = 'Hevia', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'David', @AuthorFName = 'Peralta', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Daniel', @AuthorFName = 'Peralta', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rodrigo', @AuthorFName = 'Telgie', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Cecilia', @AuthorFName = 'Morgado', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ximena', @AuthorFName = 'González', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pablo', @AuthorFName = 'Chávez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Andrei', @AuthorFName = 'Tchernitchin', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rodrigo', @AuthorFName = 'Bustamante', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Silvia', @AuthorFName = 'Erazo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Leonardo', @AuthorFName = 'Gaete', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rubén', @AuthorFName = 'García', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Igor', @AuthorFName = 'Lemus', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Luis', @AuthorFName = 'Quiñones', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Iván', @AuthorFName = 'Saavedra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Joan', @AuthorFName = 'Villena', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '47_4_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_1_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Daisy', @AuthorFName = 'Vidal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_1_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mercedes', @AuthorFName = 'Zavala', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_1_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Manuel', @AuthorFName = 'Castro', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_1_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Olga', @AuthorFName = 'Mora', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_1_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'María', @AuthorFName = 'Mathiesen', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_1_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Camilo', @AuthorFName = 'Cid', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ximena', @AuthorFName = 'Riesco', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Manuel', @AuthorFName = 'Zúñiga', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Yuri', @AuthorFName = 'Carvajal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Andrés', @AuthorFName = 'Heerlein', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_2_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Ibacache', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_2_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_2_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Molina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Cristian', @AuthorFName = 'Herrera', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alejandra', @AuthorFName = 'Fuentes', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Electra', @AuthorFName = 'González', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_2_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Temístocles', @AuthorFName = 'Molina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_2_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ingrid', @AuthorFName = 'Leal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pablo', @AuthorFName = 'Rodríguez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_3_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_3_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Camilo', @AuthorFName = 'Cid', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pere', @AuthorFName = 'Ibern', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Ibacache', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alejandra', @AuthorFName = 'Leighton', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Daniela', @AuthorFName = 'Monsalve', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Jadresic', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '48_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_1.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Osvaldo', @AuthorFName = 'Artaza', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Manuel', @AuthorFName = 'Ipinza', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Armando', @AuthorFName = 'Arredondo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Camilo', @AuthorFName = 'Cid', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ximena', @AuthorFName = 'Riesco', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Camilo', @AuthorFName = 'Cid', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Ríos', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Enrique', @AuthorFName = 'Paris', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Daniel', @AuthorFName = 'Maqueira', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Mieres', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marli', @AuthorFName = 'Bettini', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Patricia', @AuthorFName = 'Cerda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marcela', @AuthorFName = 'Pérez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Tamara De', @AuthorFName = 'la', @AuthorMName = 'Barra', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Victoria', @AuthorFName = 'Bravo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sandra', @AuthorFName = 'Solari', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = 'Sánchez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_1_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Comité', @AuthorFName = 'editor', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_2_1.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Nelson', @AuthorFName = 'Vargas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_2_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Renzo', @AuthorFName = 'Tassara', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_2_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Miguel', @AuthorFName = 'Domínguez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_2_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Miguel', @AuthorFName = 'Puxan', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_2_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Mendoza', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_2_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rossana', @AuthorFName = 'Faúndez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_2_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Miguel', @AuthorFName = 'Kottow', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_2_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Yuri', @AuthorFName = 'Carvajal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_2_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Patricio', @AuthorFName = 'Hevia', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Yuri', @AuthorFName = 'Carvajal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = 'Sánchez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_2_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Comité', @AuthorFName = 'Editor', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Claudio', @AuthorFName = 'Sepúlveda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_2_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Comité', @AuthorFName = 'Editor', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_3_1.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Víctor', @AuthorFName = 'Peña', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_3_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Sociedad Chilena de Administradores en Atención Médica y Hospitalaria', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_3_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Valenzuela', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Miguel', @AuthorFName = 'Kottow', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Yuri', @AuthorFName = 'Carvajal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Nelson', @AuthorFName = 'Vargas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Isabe', @AuthorFName = 'Álvarez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Osvaldo', @AuthorFName = 'Artaza', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Yalile', @AuthorFName = 'Osman', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Morales', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Verónica', @AuthorFName = 'Monreal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_3_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Álvaro', @AuthorFName = 'Erazo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Nelson', @AuthorFName = 'Vargas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_3_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Antonio', @AuthorFName = 'Cavalla', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_3_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Octavio', @AuthorFName = 'Enríqyez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_4_1.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mario', @AuthorFName = 'Parada', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_4_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Gregoire', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_4_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Dunny', @AuthorFName = 'Casanova', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_4_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alma', @AuthorFName = 'Vidales', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_4_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carolina', @AuthorFName = 'Jara', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Andrei', @AuthorFName = 'Tchernitchin', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rodrigo', @AuthorFName = 'Bustamante', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Leonardo', @AuthorFName = 'Gaete', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Andrei', @AuthorFName = 'Tchernitchin', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_4_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ri-Ting', @AuthorFName = 'Lin', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Chang-Chuan', @AuthorFName = 'Chan', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '49_4_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '50_1_1.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = 'Sánchez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '50_1_2.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Iván', @AuthorFName = 'Saavedra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '50_1_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sylvia Santander', @AuthorFName = 'Jorge', @AuthorMName = 'Sánchez', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '50_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Luis', @AuthorFName = 'Zapata', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '50_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sebatián', @AuthorFName = 'Sánchez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '50_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alejandra', @AuthorFName = 'Chávez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '50_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Daisy', @AuthorFName = 'Vidal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '50_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'José', @AuthorFName = 'Merino', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '50_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Becerra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '50_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END
