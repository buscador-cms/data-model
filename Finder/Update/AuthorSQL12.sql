USE Finder;
DECLARE @ID BIGINT
DECLARE @AID BIGINT

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rodolfo', @AuthorFName = 'Armas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Isidoro', @AuthorFName = 'Tohá', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Lionel', @AuthorFName = 'Bernier', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'José', @AuthorFName = 'Ugarte', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mariano', @AuthorFName = 'Bravo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gabriela', @AuthorFName = 'Venturini', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Akel', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = 'Puente', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Miguel', @AuthorFName = 'González', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Lautaro', @AuthorFName = 'Campos', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ramiro', @AuthorFName = 'Olivares', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Luis', @AuthorFName = 'Niedmann', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gonzalo', @AuthorFName = 'Grebe', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Armando', @AuthorFName = 'González', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Isabel', @AuthorFName = 'Segovia', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alejandro', @AuthorFName = 'Goic', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Molina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hernán', @AuthorFName = 'Sandoval', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ximena', @AuthorFName = 'Díaz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'Pino', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Clelia', @AuthorFName = 'Vallebuona', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Orielle', @AuthorFName = 'Solar', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Sepúlveda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_1_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Guillermo', @AuthorFName = 'Solar', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_1_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ricardo', @AuthorFName = 'Bize', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_1_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Iván', @AuthorFName = 'Serra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_1_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Miranda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_1_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Claudia', @AuthorFName = 'Segovia', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_1_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Catalina', @AuthorFName = 'Domecq', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_1_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mónica', @AuthorFName = 'Vargas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_1_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Igor', @AuthorFName = 'Lemus', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_1_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Pemjean', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Douglas', @AuthorFName = 'Bennett', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marco', @AuthorFName = 'Barrientos', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rafael', @AuthorFName = 'Sepúlveda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Reinaldo', @AuthorFName = 'Bustos', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Maass', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hernán', @AuthorFName = 'Monasterio', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rafael', @AuthorFName = 'Céspedes', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Dìaz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Maass', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hernán', @AuthorFName = 'Monasterio', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fernando', @AuthorFName = 'Voigt', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Héctor', @AuthorFName = 'Camerati', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Andrés', @AuthorFName = 'Martínez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Lorena', @AuthorFName = 'Miranda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Rodríguez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Teresita', @AuthorFName = 'Serrano', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Macarena', @AuthorFName = 'Valdés', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ramón', @AuthorFName = 'Florenzano', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Arturo', @AuthorFName = 'Roizblatt', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Labra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Cecilia', @AuthorFName = 'Canto', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jocelyn', @AuthorFName = 'Slimming', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marcela', @AuthorFName = 'López', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pablo', @AuthorFName = 'Lara', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edith', @AuthorFName = 'Thomas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Luis', @AuthorFName = 'Alvarado', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Aladino', @AuthorFName = 'Rojas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Luis', @AuthorFName = 'Aracena', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ana', @AuthorFName = 'Kaempffer', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edith', @AuthorFName = 'Cornejo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Enrique', @AuthorFName = 'Hernández', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ximena', @AuthorFName = 'Díaz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_15.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Julia', @AuthorFName = 'Medel', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_15.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Norah', @AuthorFName = 'Schlaen', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_15.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_2_16.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ninette', @AuthorFName = 'Posek', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ana', @AuthorFName = 'Villarino', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Paulina', @AuthorFName = 'Saldías', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Beatriz', @AuthorFName = 'Gazmuri', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Remigio', @AuthorFName = 'Muñoz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Adriana', @AuthorFName = 'Dreckmann', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alberto', @AuthorFName = 'Minoletti', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maritza', @AuthorFName = 'Bocic', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ana', @AuthorFName = 'Kaempffer', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edith', @AuthorFName = 'Cornejo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Enrique', @AuthorFName = 'Hernández', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Hernández', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Christian', @AuthorFName = 'Daszenies', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Aníbal', @AuthorFName = 'Aguilera', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'César', @AuthorFName = 'Pinilla', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'López', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'José', @AuthorFName = 'Hono', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Cristian', @AuthorFName = 'Daszenies', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Cecilia', @AuthorFName = 'Westermayer', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'César', @AuthorFName = 'Pinilla', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Hernández', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'López', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edith', @AuthorFName = 'Cornejo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Raquel', @AuthorFName = 'Carrasco', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Rojas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ana', @AuthorFName = 'Kaempffer', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Enrique', @AuthorFName = 'Hernández', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_3_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marcelo', @AuthorFName = 'Wolff', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rebeca', @AuthorFName = 'Northland', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rebeca', @AuthorFName = 'Northland', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Félix', @AuthorFName = 'Fich', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_4_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jeannette', @AuthorFName = 'Dabanch', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Llambías', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_4_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Llambías', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_4_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = 'Zorrilla', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_4_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_4_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Manuel', @AuthorFName = 'Gutierrez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_4_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Elías', @AuthorFName = 'Apud', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_4_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Comisión Nacional', @AuthorFName = 'del', @AuthorMName = 'SIDA-Chile', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '37_4_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mariano', @AuthorFName = 'Requena', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Oscar', @AuthorFName = 'Ramón', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Francisco', @AuthorFName = 'Espejo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Roberto', @AuthorFName = 'Fuentes', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marie', @AuthorFName = 'Bouësseau', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carmen', @AuthorFName = 'Aravena', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sandra', @AuthorFName = 'Madrid', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Silvia', @AuthorFName = 'Riquelme', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Enrique', @AuthorFName = 'Baritari', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sandra', @AuthorFName = 'Madrid', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Enrique', @AuthorFName = 'Barilari', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Cortés', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alejandro', @AuthorFName = 'Guerra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'J.', @AuthorFName = 'Román', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Saba', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Andrés', @AuthorFName = 'Casassas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Paulina', @AuthorFName = 'González', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Cecilia', @AuthorFName = 'Zapata', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Noelia', @AuthorFName = 'Betancur', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Llambias-Wolff', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_1_15.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Reinaldo', @AuthorFName = 'Bustos', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_2_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Noëlle', @AuthorFName = 'Lenoir', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_2_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = 'Zorrilla', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fermín', @AuthorFName = 'Roland', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pablo', @AuthorFName = 'Rodríguez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marie', @AuthorFName = 'Bouësseau', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '38_2_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END
