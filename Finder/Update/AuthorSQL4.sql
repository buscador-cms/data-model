USE Finder;
DECLARE @ID BIGINT
DECLARE @AID BIGINT

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fernanda', @AuthorFName = 'Mesquida', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carmen', @AuthorFName = 'Naveilán', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'Campusano', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Naveillán', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Paz', @AuthorFName = 'Zulic', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Agustín', @AuthorFName = 'Gallardo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Sánchez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Delia', @AuthorFName = 'Bravo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'Concha', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hernán', @AuthorFName = 'Venturino', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'otros', @AuthorFName = 'autores', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Francisco', @AuthorFName = 'Menchaca', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_2_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Salud Pública y', @AuthorFName = 'Bienestar', @AuthorMName = 'Social', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ana', @AuthorFName = 'Kaempffer', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_3_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_3_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'de la Fuente', @AuthorMName = 'Fuente', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_3_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'Romero', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_3_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'José', @AuthorFName = 'Ugarte', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Julio', @AuthorFName = 'Basoalto', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ana', @AuthorFName = 'Kaempffer', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'Romero', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Nicolás', @AuthorFName = 'Majluf', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Guacolda', @AuthorFName = 'Ubilla', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mariano', @AuthorFName = 'Guerrero', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gonzalo', @AuthorFName = 'Palacios', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'de la Fuente', @AuthorMName = 'Fuente', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Julia', @AuthorFName = 'González', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hugo', @AuthorFName = 'Muñoz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fernando', @AuthorFName = 'Muñoz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gladys', @AuthorFName = 'Yentzen', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Matías', @AuthorFName = 'Tijmes.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Avendaño', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_4_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'Romero', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Nicolás', @AuthorFName = 'Majluf', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Guacolda', @AuthorFName = 'Ubilla', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mariano', @AuthorFName = 'Guerrero', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gonzalo', @AuthorFName = 'Palacios', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '17_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Naveillán', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Filomena', @AuthorFName = 'Falaha', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Manuela', @AuthorFName = 'Vicente', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edith', @AuthorFName = 'Aguilera', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Honold', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hernán', @AuthorFName = 'Montenegro', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Naveillán', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Retamal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hernán', @AuthorFName = 'Oyanguren', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Santiago', @AuthorFName = 'Urcelay', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_2_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'María', @AuthorFName = 'Thomas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_2_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Myrtha', @AuthorFName = 'Sims', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_2_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Naveillán', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carmen', @AuthorFName = 'Steinert', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Verónica', @AuthorFName = 'Valdenegro', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Luis', @AuthorFName = 'Espinoza', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Ubilla', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Avendaño', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Domingo', @AuthorFName = 'Asún', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Patri', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Patricia', @AuthorFName = 'Hamel', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'María', @AuthorFName = 'de la Fuente', @AuthorMName = 'Fuente', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Cecilia', @AuthorFName = 'Albala', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ramiro', @AuthorFName = 'Molina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'López', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marcial', @AuthorFName = 'Orellana', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Orlandi', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Patricio', @AuthorFName = 'Mena', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'José', @AuthorFName = 'Ugarte', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_4_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = 'Álvarez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '18_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ramiro', @AuthorFName = 'Molina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'López', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marcial', @AuthorFName = 'Orellana', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Orlandi', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Julia', @AuthorFName = 'Alarcón', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'C.', @AuthorFName = 'Naveillán', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'M.', @AuthorFName = 'Campusano', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'P.', @AuthorFName = 'Zulic', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'P.', @AuthorFName = 'Naveillán', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'M.', @AuthorFName = 'Frauenberg', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'M.', @AuthorFName = 'Fruns', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Germán', @AuthorFName = 'Guerra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Bombil', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Lorenzo', @AuthorFName = 'Naranjo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Elba', @AuthorFName = 'Olivares', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Roberto', @AuthorFName = 'Von Dessauer', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_2_3.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carmen', @AuthorFName = 'Velasco', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_2_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'José', @AuthorFName = 'Ugarte', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_2_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Daniel', @AuthorFName = 'Juricic', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'C.', @AuthorFName = 'Naveillán', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'M.', @AuthorFName = 'Campusano', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'P.', @AuthorFName = 'Zulic', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'P.', @AuthorFName = 'Naveillán', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'M.', @AuthorFName = 'Frauenberg', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'M.', @AuthorFName = 'Fruns', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Patri', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Guillermo', @AuthorFName = 'Adriasola', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marcel', @AuthorFName = 'Trucco', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Retamal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_3_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Naveillán', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Adela', @AuthorFName = 'Legarreta', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Silvia', @AuthorFName = 'Pessoa', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mario', @AuthorFName = 'Sepúlveda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Héctor', @AuthorFName = 'Sánchez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ilse', @AuthorFName = 'López', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_3_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Elcira', @AuthorFName = 'Galdames', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_3_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = 'Vargas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_3_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Miriam', @AuthorFName = 'Castellanos', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_3_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'José', @AuthorFName = 'Ugarte', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mirella', @AuthorFName = 'de', @AuthorMName = 'Kartzow', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Margozzini', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_4_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'colaboradores', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_4_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Horacio', @AuthorFName = 'Boccardo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Daniel', @AuthorFName = 'Juricic', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Saleh', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '19_4_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Norambuena', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hugo', @AuthorFName = 'Muñoz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ramón', @AuthorFName = 'Florenzano', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Oscar', @AuthorFName = 'Feuerhake', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Agustín', @AuthorFName = 'Gallardo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fresia', @AuthorFName = 'Solis', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Solicitada', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carmen', @AuthorFName = 'Velasco', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Kamps', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Nora', @AuthorFName = 'Cabrera.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_2_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Perez-Olea', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Myriam', @AuthorFName = 'Castellanos', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ilse', @AuthorFName = 'López', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Margozzini', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Nora', @AuthorFName = 'Palacios', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Patri', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Haydee', @AuthorFName = 'Sepúlveda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Valenzuela', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'colaboradores', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Germán', @AuthorFName = 'Corey', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Angele', @AuthorFName = 'Petros-Barvazian', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'M.', @AuthorFName = 'Behar', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carmen', @AuthorFName = 'Velasco', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'José', @AuthorFName = 'Ugarte', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '20_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END
