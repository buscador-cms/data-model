USE Finder;
DECLARE @ID BIGINT
DECLARE @AID BIGINT

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Estela', @AuthorFName = 'Arcos', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Julia', @AuthorFName = 'Cortés', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Adriana', @AuthorFName = 'Olivo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juana', @AuthorFName = 'Romero', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jacqueline', @AuthorFName = 'Saldivia', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Dagmar', @AuthorFName = 'Holmgren', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_2_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Victor', @AuthorFName = 'Lermanda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_2_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Vera', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_2_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'César', @AuthorFName = 'Oyarzo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_2_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Luna', @AuthorFName = 'Israel', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_2_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ana', @AuthorFName = 'Kaempffer', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edith', @AuthorFName = 'Cornejo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Enrique', @AuthorFName = 'Hernández', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rafael', @AuthorFName = 'Yáñez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Claudia', @AuthorFName = 'Carvajal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Patricia', @AuthorFName = 'Cárcamo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carmen', @AuthorFName = 'Garrido', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sonia', @AuthorFName = 'Olivares', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Margarita', @AuthorFName = 'Andrade', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rafael', @AuthorFName = 'Sepúlveda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Iván', @AuthorFName = 'Serra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Filomena', @AuthorFName = 'Falaha', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'María', @AuthorFName = 'Soto', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Luis', @AuthorFName = 'Rodríguez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Enrique', @AuthorFName = 'Cancec', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Miranda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Clara', @AuthorFName = 'Misrachi', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rocio', @AuthorFName = 'Sapag', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Milka', @AuthorFName = 'Kaplán', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Macarena', @AuthorFName = 'Valdés', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Román', @AuthorFName = 'Florenzano', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Paulina', @AuthorFName = 'Pino', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Burrows', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Nudman', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Delfina', @AuthorFName = 'López', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Delia', @AuthorFName = 'Soto', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Marconi', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = 'Cecchetto', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alberto', @AuthorFName = 'Estévez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_3_15.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Giaconi', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gonzalo', @AuthorFName = 'Valdivia', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eugenia', @AuthorFName = 'Arteaga', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hugo', @AuthorFName = 'Pumarino', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ana', @AuthorFName = 'Kaempffer', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edith', @AuthorFName = 'Cornejo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Enrique', @AuthorFName = 'Hernández', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Davis', @AuthorFName = 'Benavente', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Inés', @AuthorFName = 'Salas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Valeria', @AuthorFName = 'Calderón', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Daniel', @AuthorFName = 'Muñoz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Lorena', @AuthorFName = 'Valdebenito', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Manuel', @AuthorFName = 'Castro', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mercedes', @AuthorFName = 'Zavala', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Olivia', @AuthorFName = 'Sanhueza', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Soledad', @AuthorFName = 'Villaseñor', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sussy', @AuthorFName = 'Werner', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Santiago', @AuthorFName = 'Ibacache', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'John', @AuthorFName = 'Araneda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'José', @AuthorFName = 'Casassus', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Paulina', @AuthorFName = 'González', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Arévalo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Peña', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ministerio', @AuthorFName = 'de', @AuthorMName = 'Salud', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Colegio', @AuthorFName = 'Médico', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Federación de', @AuthorFName = 'Colegios', @AuthorMName = 'Profesionales', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Escuela de Salud Pública y Universidades', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sociedad Chilena', @AuthorFName = 'de', @AuthorMName = 'Salubridad', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Organización Panamericana', @AuthorFName = 'de', @AuthorMName = 'Salud.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '35_4_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Estela', @AuthorFName = 'Arcos', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Giaconi', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Claudia', @AuthorFName = 'Valenzuela', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Catalina', @AuthorFName = 'Domecq', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mónica', @AuthorFName = 'Vargas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'John', @AuthorFName = 'Araneda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'José', @AuthorFName = 'Casassas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Paulina', @AuthorFName = 'González', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Santiago', @AuthorFName = 'Ibacache', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mariano', @AuthorFName = 'Requena', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_1_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Depto.', @AuthorFName = 'Salud', @AuthorMName = 'Pública', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_1_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ana', @AuthorFName = 'Kaempffer', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edith', @AuthorFName = 'Cornejo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Enrique', @AuthorFName = 'Hernández', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Margarita', @AuthorFName = 'Fernández', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Atalah', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Manuel', @AuthorFName = 'Castro', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'Mathiesen', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Olga', @AuthorFName = 'Mora', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Rioseco', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mariana', @AuthorFName = 'Quijada', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Julio', @AuthorFName = 'Salgado', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'John', @AuthorFName = 'Araneda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Santiago', @AuthorFName = 'Ibacache', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Orlando', @AuthorFName = 'Ahumada', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = 'Tapia', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Fernández', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rubén', @AuthorFName = 'Alvarado', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Patricia', @AuthorFName = 'Vásquez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Silvia', @AuthorFName = 'Soto', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Julia', @AuthorFName = 'Pérez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Silvia', @AuthorFName = 'Raffo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'Concha', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = 'Varas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'José', @AuthorFName = 'Caballero', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fernando', @AuthorFName = 'Véliz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Depto de', @AuthorFName = 'Salud', @AuthorMName = 'Pública', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Consejo General Colegio Médico de Chile.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_2_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Ramírez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Catalina', @AuthorFName = 'Domecq', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alma', @AuthorFName = 'Belmar', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Manuel', @AuthorFName = 'Gutierrez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Elías', @AuthorFName = 'Apud', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Heidy', @AuthorFName = 'Leiva', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sally', @AuthorFName = 'Santori', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Claudia', @AuthorFName = 'Hernández', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Rojas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Raquel', @AuthorFName = 'Carrasco', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edith', @AuthorFName = 'Cornejo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Liliana', @AuthorFName = 'Cortés', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Angélica', @AuthorFName = 'Orb', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'M', @AuthorFName = 'Medanic', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'I', @AuthorFName = 'Serra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'M De', @AuthorFName = 'la', @AuthorMName = 'Fuente', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'L', @AuthorFName = 'Marchant', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fernando', @AuthorFName = 'Espina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Nina', @AuthorFName = 'Tchernitchin', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Lionel', @AuthorFName = 'Gil', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marta', @AuthorFName = 'Adonis', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Andrei', @AuthorFName = 'Tchernitchin', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_4_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Adriano', @AuthorFName = 'Morales', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_4_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'J', @AuthorFName = 'Torres', @AuthorMName = 'Pereyra', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_4_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'J', @AuthorFName = 'Torres', @AuthorMName = 'Torreti', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_4_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'María', @AuthorFName = 'Rivara', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_4_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Germán', @AuthorFName = 'Corey', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_4_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'Rivara', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_4_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Lionel', @AuthorFName = 'Bernier', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Carabantes', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Rosselot', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '36_5_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END
