USE Finder;
DECLARE @ID BIGINT
DECLARE @AID BIGINT

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rosa', @AuthorFName = 'Silva', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '25_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Waldo', @AuthorFName = 'Etcheberry', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '25_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Oscar', @AuthorFName = 'Gamarra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '25_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Lobos', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '25_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Misael', @AuthorFName = 'Lopetegui', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '25_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '25_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Aliaga', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '25_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Anabalón', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '25_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rossana', @AuthorFName = 'Díaz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '25_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fernando', @AuthorFName = 'González', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '25_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marcela', @AuthorFName = 'Labbé', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '25_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Adriana', @AuthorFName = 'Merchak', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '25_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Roberto', @AuthorFName = 'Miranda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '25_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ramón', @AuthorFName = 'Florenzano', @AuthorMName = 'Urzúa', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '25_4_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mario', @AuthorFName = 'Montecinos', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Claudio', @AuthorFName = 'Espejo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Zúñiga', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rodrigo Ponce', @AuthorFName = 'de', @AuthorMName = 'Luca', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rosa', @AuthorFName = 'Inostroza', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'José', @AuthorFName = 'Ugarte', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Onofre', @AuthorFName = 'Avendaño', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pilar', @AuthorFName = 'Fernández', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Lionel', @AuthorFName = 'Bernier', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mario', @AuthorFName = 'Ferreiro', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mafalda', @AuthorFName = 'Rizzardini', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Tapia', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Raúl', @AuthorFName = 'Tapia.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Victoria', @AuthorFName = 'Gassibe', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gonzalo', @AuthorFName = 'Azócar', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Luis', @AuthorFName = 'Ammann', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'César', @AuthorFName = 'Jara', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'David', @AuthorFName = 'Kutz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Freddy', @AuthorFName = 'Palma', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marco', @AuthorFName = 'Olguín', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Boris', @AuthorFName = 'Kuzmicic', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gabriela', @AuthorFName = 'Venturini', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Patricio', @AuthorFName = 'Peñailillo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eugenia', @AuthorFName = 'Valdés', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Cristián', @AuthorFName = 'Wulff', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Atilio', @AuthorFName = 'Rébori', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mario', @AuthorFName = 'Fuentealba', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_2_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ana', @AuthorFName = 'Kaempffer', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Francisco', @AuthorFName = 'Cumsille', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Raquel', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Giaconi', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Marín', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ilse', @AuthorFName = 'López', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Silvia', @AuthorFName = 'Pessoa', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Diego', @AuthorFName = 'Salazar', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'colaboradores', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Guillermo', @AuthorFName = 'Adriasola', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Daniel', @AuthorFName = 'Alvo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Justiniano', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Germán', @AuthorFName = 'Larraín', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Consejo Editorial de Cuadernos Médico Sociales.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_3_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Julia', @AuthorFName = 'González', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Tatiana', @AuthorFName = 'Tironi', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Luz', @AuthorFName = 'Parodi', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Adriana', @AuthorFName = 'Vargas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marcela', @AuthorFName = 'Oyarzún', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Numair', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gustavo', @AuthorFName = 'Hein', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Nelson', @AuthorFName = 'Vargas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Solange', @AuthorFName = 'Valenzuela', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Catenacci', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marión', @AuthorFName = 'Emhart', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carolina', @AuthorFName = 'García', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Gómez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ricardo', @AuthorFName = 'González', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marcelo', @AuthorFName = 'Guzmán', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Verónica', @AuthorFName = 'Muñoz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ana', @AuthorFName = 'Repetto', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Elena', @AuthorFName = 'Carrasco', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Manuel', @AuthorFName = 'García', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'María', @AuthorFName = 'Riesco', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Beatriz', @AuthorFName = 'Fonseca', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gloria', @AuthorFName = 'López', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'María', @AuthorFName = 'Valladares', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Iris', @AuthorFName = 'Mella', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'José', @AuthorFName = 'Ugarte', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '26_4_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Iván', @AuthorFName = 'Serra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Raúl', @AuthorFName = 'Zapata', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Galvarino', @AuthorFName = 'Pérez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gabriela', @AuthorFName = 'Venturini', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = 'Acevedo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Viviana', @AuthorFName = 'Albornoz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mabel', @AuthorFName = 'Arinoviche', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rodolfo', @AuthorFName = 'Armas', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Cecilia', @AuthorFName = 'Breinbahuer', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Cox', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Flavia', @AuthorFName = 'Garbin', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mauricio', @AuthorFName = 'González', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rodrigo', @AuthorFName = 'Guridi', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marisol', @AuthorFName = 'Gutierrez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_8.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Iván', @AuthorFName = 'Serra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_9.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Iván', @AuthorFName = 'Serra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_1_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fresia', @AuthorFName = 'Solis', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Graciela', @AuthorFName = 'Mardones', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Berta', @AuthorFName = 'Castillo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Iván', @AuthorFName = 'Serra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfonso', @AuthorFName = 'Calvo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Allan', @AuthorFName = 'Sharp', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rogelio', @AuthorFName = 'Aravena', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marcel', @AuthorFName = 'Jacob', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sylvia', @AuthorFName = 'Raffo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Betty', @AuthorFName = 'Gómez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Virginia', @AuthorFName = 'Guerrero', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alicia', @AuthorFName = 'Sagaceta', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mario', @AuthorFName = 'Varela', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Evaristo', @AuthorFName = 'Bustamante', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'César', @AuthorFName = 'Jara', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Santibáñez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_2_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'María', @AuthorFName = 'Yáñez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Néstor', @AuthorFName = 'Romero', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Claudio', @AuthorFName = 'Ramírez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edgardo', @AuthorFName = 'Sánchez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = 'Zamora', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_3_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Leticia', @AuthorFName = 'Marzolo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ilse', @AuthorFName = 'López', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Aída', @AuthorFName = 'Kirshbaum', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_3_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marcela', @AuthorFName = 'Barría', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Estela', @AuthorFName = 'Gallardo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Patricia', @AuthorFName = 'Guarda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Amanda', @AuthorFName = 'Herrera', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Cecilia', @AuthorFName = 'Soto', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Schiefelbein', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_3_6.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Guillermo', @AuthorFName = 'Adriasola', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_3_7.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Patricio', @AuthorFName = 'Silva', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hernán', @AuthorFName = 'Montenegro', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Prado', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fernando', @AuthorFName = 'González', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alberto', @AuthorFName = 'Laporte', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Prado', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_4_4.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Julia', @AuthorFName = 'González', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Ventura', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Miguel', @AuthorFName = 'Villaseca', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Vladimir', @AuthorFName = 'Yuseff', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Flavio', @AuthorFName = 'Zambra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Francisco', @AuthorFName = 'Zúñiga', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '27_4_5.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID AND AuthorID = @ID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END
