USE Finder
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Nicol�s Mancilla
-- Create date: 
-- Description:	Creates an article author.
-- =============================================
IF OBJECT_ID('FinderSchema.cms_CreateAuthor', 'P') IS NOT NULL
	DROP PROCEDURE FinderSchema.cms_CreateAuthor;
GO

CREATE PROCEDURE FinderSchema.cms_CreateAuthor
	-- Add the parameters for the stored procedure here
	@AuthorFName VARCHAR(100),
	@AuthorMName VARCHAR(100),
	@AuthorName VARCHAR(100),
	@AuthorID BIGINT = NULL OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	SET @AuthorID = (SELECT AuthorID
		FROM FinderSchema.Author
		WHERE AuthorFName = @AuthorFName AND AuthorMName = @AuthorMName AND AuthorName = @AuthorName);

	IF @AuthorID IS NULL
	BEGIN
		INSERT INTO FinderSchema.Author (AuthorFName, AuthorMName, AuthorName)
		VALUES (@AuthorFName, @AuthorMName, @AuthorName);
		SET @AuthorID = (SELECT SCOPE_IDENTITY());
	END
END
GO

IF OBJECT_ID('FinderSchema.cms_UpdateAuthor', 'P') IS NOT NULL
	DROP PROCEDURE FinderSchema.cms_UpdateAuthor
GO

CREATE PROCEDURE FinderSchema.cms_UpdateAuthor
	@AuthorFName VARCHAR(100),
	@AuthorMName VARCHAR(100),
	@AuthorName VARCHAR(100),
	@AuthorID BIGINT
AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT AuthorID FROM FinderSchema.Author WHERE (AuthorFName + ' ' + AuthorMName) = (@AuthorFName + ' ' + @AuthorFName) AND AuthorID = @AuthorID)
	BEGIN
		UPDATE FinderSchema.Author SET 
			AuthorFName = @AuthorFName,
			AuthorMName = @AuthorMName,
			AuthorName = @AuthorName
		WHERE AuthorID = @AuthorID;
	END
END
GO

-- =============================================
-- Author:		Nicol�s Mancilla
-- Create date: 
-- Description:	Creates an article author.
-- =============================================
IF OBJECT_ID('FinderSchema.cms_CreateKeyword', 'P') IS NOT NULL
	DROP PROCEDURE FinderSchema.cms_CreateKeyword;
GO

CREATE PROCEDURE FinderSchema.cms_CreateKeyword
	@KeywordValue VARCHAR(200),
	@KeywordID BIGINT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	SET @KeywordID = (SELECT KeywordID FROM FinderSchema.Keyword WHERE KeywordValue = @KeywordValue);

	IF @KeywordID IS NULL
	BEGIN
		INSERT INTO FinderSchema.Keyword (KeywordValue) VALUES (@KeywordValue);
		SET @KeywordID = (SELECT SCOPE_IDENTITY());
	END
END
GO

IF OBJECT_ID('FinderSchema.cms_UpdateKeyword', 'P') IS NOT NULL
	DROP PROCEDURE FinderSchema.cms_UpdateKeyword;
GO

CREATE PROCEDURE FinderSchema.cms_UpdateKeyword
	@KeywordValue VARCHAR(500),
	@KeywordID BIGINT
AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS (SELECT KeywordID FROM FinderSchema.Keyword WHERE KeywordID = @KeywordID)
	BEGIN
		UPDATE FinderSchema.Keyword SET KeywordValue = @KeywordValue
		WHERE KeywordID = @KeywordID;
	END
END
GO

-- =============================================
-- Author:		Nicol�s Mancilla
-- Create date: 
-- Description:
-- =============================================
IF OBJECT_ID('FinderSchema.cms_CreateMagazine', 'P') IS NOT NULL
	DROP PROCEDURE FinderSchema.cms_CreateMagazine;
GO

CREATE PROCEDURE FinderSchema.cms_CreateMagazine
	@MagazineName VARCHAR = '',
	@MagazineNumber BIGINT,
	@PublishYear INT,
	@MagazineID BIGINT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	SET @MagazineID = (SELECT MagazineID FROM FinderSchema.Magazine WHERE (PublishYear = @PublishYear AND MagazineNumber = @MagazineNumber))

	IF @MagazineID IS NULL
	BEGIN
		IF @MagazineName IS NULL
			SET @MagazineName = '';

		INSERT INTO FinderSchema.Magazine (MagazineNumber, MagazineName, PublishYear)
		VALUES (@MagazineNumber, @MagazineName, @PublishYear);
		SET @MagazineID = (SELECT SCOPE_IDENTITY());
	END
END
GO

IF OBJECT_ID('FinderSchema.cms_UpdateMagazine', 'P') IS NOT NULL
	DROP PROCEDURE FinderSchema.cms_UpdateMagazine;
GO

CREATE PROCEDURE FinderSchema.cms_UpdateMagazine
	@MagazineName VARCHAR,
	@MagazineNumber BIGINT,
	@PublishYear INT,
	@MagazineID BIGINT
AS
BEGIN
	SET NOCOUNT ON;

	IF @MagazineID IS NOT NULL
		IF @MagazineName IS NULL
			SET @MagazineName = '';

		UPDATE FinderSchema.Magazine SET
			MagazineName = @MagazineName,
			MagazineNumber = @MagazineNumber,
			PublishYear = @PublishYear
		WHERE MagazineID = @MagazineID;
END
GO

-- =============================================
-- Author:		Nicol�s Mancilla
-- Create date: 
-- Description:	Creates an article author.
-- =============================================


IF OBJECT_ID('FinderSchema.cms_CreateArticle', 'P') IS NOT NULL
	DROP PROCEDURE FinderSchema.cms_CreateArticle;
GO

CREATE PROCEDURE FinderSchema.cms_CreateArticle
	@Number INT,
	@Title VARCHAR(MAX),
	@File VARCHAR(500),
	@Desc VARCHAR(MAX) = '',
	@PageCnt INT,
	@MagID BIGINT,
	@ID BIGINT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	SET @ID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = @File AND ArticleTitle = @Title);

	IF (@ID) IS NULL
	BEGIN
		IF @Desc IS NULL
			SET @Desc = '';

		INSERT INTO FinderSchema.Article (ArticleNumber, ArticleTitle, ArticleFile, ArticleDesc, ArticlePageCount, MagazineID)
			VALUES (@Number, @Title, @File, @Desc, @PageCnt, @MagID)
		SET @ID = (SELECT SCOPE_IDENTITY());
	END
END
GO

IF OBJECT_ID('FinderSchema.cms_UpdatePreference', 'P') IS NOT NULL
	DROP PROCEDURE FinderSchema.cms_UpdatePreference;
GO

CREATE PROCEDURE FinderSchema.cms_UpdatePreference
	@PrefName VARCHAR(500),
	@PrefValue VARCHAR(500),
	@PrefID BIGINT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	SET @PrefID = (SELECT PrefID FROM FinderSchema.Preference WHERE PrefName = @PrefName);

	IF @PrefID IS NOT NULL
		UPDATE FinderSchema.Preference SET PrefValue = @PrefValue WHERE PrefID = @PrefID;
	ELSE
	BEGIN
		INSERT INTO FinderSchema.Preference (PrefName, PrefValue) VALUES (@PrefName, @PrefValue);
		SET @PrefID = (SELECT SCOPE_IDENTITY());
	END
END
GO