USE Finder;

INSERT INTO FinderSchema.Magazine (PublishYear, MagazineNumber, MagazineName) VALUES (1959, 1, 'none'),
(1960, 1, 'none'),
(1961, 2, 'none'),
(1962, 3, 'none'),
(1962, 4, 'none'),
(1963, 4, 'none'),
(1964, 5, 'none'),
(1964, 0, 'none'),
(1965, 6, 'none'),
(1966, 7, 'none'),
(1967, 8, 'none'),
(1968, 9, 'none'),
(1969, 10, 'none'),
(1970, 11, 'none'),
(1971, 12, 'none'),
(1972, 13, 'none'),
(1973, 14, 'none'),
(1974, 15, 'none'),
(1975, 16, 'none'),
(1976, 17, 'none'),
(1977, 18, 'none'),
(1978, 19, 'none'),
(1979, 20, 'none'),
(1979, 21, 'none');

INSERT INTO FinderSchema.Article (ArticleNumber, ArticleTitle, ArticleFile, ArticleDesc, ArticlePageCount, MagazineID) VALUES (1, '', '001_01_13.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(1, '', '001_01_14.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(1, 'Editorial', '001_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(1, 'Informe preliminar de la Comision organizadora del Seminario', '001_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(1, 'Discusi�n sobre pr�p�sitos y objetivos de la ense�anza m�dica en el momento actual.', '001_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(1, 'La medicina como ciencia social', '001_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(1, 'Objetivos y organizacion de una Escuela de medicina', '001_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(1, 'Conducta humana y sociedad.', '001_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(1, 'La educaci�n m�dica del ma�ana.', '001_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(1, 'La medicina del ma�ana', '001_01_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(1, 'El prop�sito de la educaci�n m�dica', '001_01_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(1, 'La educaci�n del cient�fico', '001_01_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(1, 'La medicina social como disciplina acad�mica.', '001_01_11.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(1, 'Tercera dimensi�n de la medicina.', '001_01_12.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, '', '001_02_20.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, '', '001_02_21.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'Editorial', '001_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'Segundo informe de la comisi�n organizadora del seminario', '001_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'Proyecciones de la escuela de sociologia de la Universidad Catolica de Chile', '001_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'La salud', '001_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'La sociolog�a de la medicina', '001_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'Sociologia m�dica: Estudio de los componentes sociales de la enfermedad y la salud.', '001_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'El soci�logo y la medicina: generalizaciones derivadas de una experiencia docente y de investigaciones en una escuela de Medicina.', '001_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'Objetivos y m�todos de la investigaci�n de la sociolog�a rural en salud en Michigan State College', '001_02_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'Objetivos y m�todos de la investigaci�n de la sociolog�a rural en salud mental en la Universidad del estado de Ohio', '001_02_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'El papel profesional del m�dico en la medicina burocratizada. Un estudio sobre conflicto de funciones.', '001_02_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'Estudios Sociol�gicos sobre la Educaci�n m�dica.', '001_02_11.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'La formacion del m�dico. Proposiciones generales de ideas y problemas.', '001_02_12.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'El efecto de la educaci�n m�dica sobre las actitudes.', '001_02_13.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'Pediatras leones y ginec�logos corderos.', '001_02_14.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'Rol de las creencias y costumbres en los programas sanitarios.', '001_02_15.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'Antropolog�a en la Educaci�n m�dica.', '001_02_16.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'Un programa de Educaci�n m�dica revisado en John Hopkins.', '001_02_17.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'Educaci�n m�dica para la medicina moderna.', '001_02_18.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(2, 'Significado de la medicina social.', '001_02_19.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, '', '001_03_17.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, '', '001_03_18.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'Editorial', '001_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'Opiniones sobre el Seminario', '001_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'Proyecto de investigaci�n en el �rea de salud p�blica', '001_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'El faro de Marblehead', '001_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'La profesi�n y la sociedad', '001_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'Informe de la comisi�n permanente Medico social sobre preparaci�n de los m�dicos en su papel preventivo y social.', '001_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'La ense�anza de la medicina social', '001_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'La ense�anza de la Medicina social. �Qui�n le pondr� el cascabel al gato?', '001_03_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'Medicina social y Educaci�n m�dica.', '001_03_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'La Educaci�n M�dica y el m�dico', '001_03_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'La misi�n del m�dico', '001_03_11.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'Ense�anza Medico social', '001_03_12.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'La ense�anza de Medicina social en la Universidad de Sheffield', '001_03_13.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'Tres a�os de experiencia en la Washington University en el programa de ense�anza coordinada con pacientes externos.', '001_03_14.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'Problemas del profesor de ense�anza precl�nica.', '001_03_15.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(3, 'Algunas consideraciones b��asicas para una revisi�n dlos estudios m�dicos.', '001_03_16.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(4, '', '001_04_14.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(4, '', '001_04_15.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(4, 'Editorial', '001_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(4, 'Ense�anza de los ramos cl�nicos', '001_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(4, 'Sobre la realizaci�n de la idea de la medicina integral en el plano educacional', '001_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(4, 'Conceptos y proposiciones sobre el tema formaci�n del m�dico.', '001_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(4, 'El concepto preventivo y social en la ense�anza m�dica.', '001_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(4, 'Horizontes crecientes de la Educaci�n m�dica.', '001_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(4, 'La formaci�n m�dica de hoy y de ma�ana.', '001_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(4, 'La investigaci�n sociol�gica en el campo de la medicina: progreso y expectativas.', '001_04_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(4, 'Comunicaciones en el equipo directivo.', '001_04_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(4, 'Cambios en las relaciones interpersonales en E. E. U. U.', '001_04_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(4, '"El papel de los ""surveys"" para elevar el conocimiento en el campo de la salud mental."', '001_04_11.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(4, 'Impresiones y observaciones sobre la ense�anza de la medicina en Chile.', '001_04_12.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(4, 'La medicina de hoy - sus prop�sitos y responsabilidades.', '001_04_13.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1959)),
(5, '', '001_05_35.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, '', '001_05_36.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'Editorial', '001_05_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'Introducci�n a los res�menes de la primera conferencia de Educaci�n m�dica.', '001_05_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'El desaf�o a la Educaci�n m�dica en la primera mitad del siglo veinte', '001_05_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'La educaci�n general en la era de la ciencia', '001_05_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'Requisitos de admisi�n en las Escuelas m�dicas.', '001_05_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'Requisitos de entrada en las escuelas de medicina en estados unidos de norte america', '001_05_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'Requisitos de entrada a las escuelas de medicina de gran breta�a', '001_05_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'La seleccion de estudiantes en Australia', '001_05_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'La seleccion de estudiantes en los estados unidos de norte america', '001_05_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'La seleccion de estudiantes en el reino unido', '001_05_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'Usos y valor de los tests de inteligencia y aptitudes.', '001_05_11.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'Introducci�n a las Ciencias Sociales.', '001_05_12.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'Propositos y contenido de los programas de ense�anza m�dica', '001_05_13.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'El equilibior en el programa de estudios', '001_05_14.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'Los propositos del curriculum de medicina', '001_05_15.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'Patologia, la ciencia cl�nica b�sica', '001_05_16.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'Estad�stica m�dica y el dise�o del experimento', '001_05_17.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'La ense�anza de medicina y cirug�a como sola disciplina', '001_05_18.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, '�Es necesario un a�o de internado?', '001_05_19.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'El estudiante de medicina yla practica general', '001_05_20.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'T�cnicas y m�todos de Educaci�n m�dica', '001_05_21.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'La ense�anza en el consultorio externo o policl�nico', '001_05_22.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'Ense�anza en el hogar', '001_05_23.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'Las clases magistrales y las discusiones de grupo', '001_05_24.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'El museo docente en la Escuela de medicina', '001_05_25.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'C�mo esnse�ar al profesor a ense�ar.', '001_05_26.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, '�Por qu� fracasan los estudiantes?', '001_05_27.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'La medicina preventiva y social', '001_05_28.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'La ense�anza de la medicina social en el periodo preclinico', '001_05_29.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'La medicina social en el periodo clinico', '001_05_30.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'Demograf�a y estadisticas vitales', '001_05_31.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'El hogar en la educaci�n m�dica', '001_05_32.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'La ense�anza de epidemiolog�a', '001_05_33.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(5, 'Integracion de la ense�anza de medicina prventiva y social en el programa de estudios medicos', '001_05_34.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(6, '', '001_06_14.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(6, '', '001_06_15.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(6, 'Editorial', '001_06_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(6, 'Sobre la segunda conferencia mundial de educaci�n', '001_06_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(6, 'Conclusiones y resultados de laprimera conferencia mundial de Educaci�n m�dica', '001_06_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(6, 'Adaptaci�n de la t�cnica de discusi�n de grupo para uso de cursos numerosos', '001_06_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(6, 'Un estudio sobre educaci�n M�dica en Estados Unidos', '001_06_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(6, 'El desaf�o de la medicina al educador', '001_06_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(6, 'factores extraescolares que afectan al estudiante de medicina', '001_06_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(6, 'El efecto de la educaci�n m�dica en las actitudes', '001_06_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(6, 'Elecci�n vocacional y evoluci�n de la carrera', '001_06_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(6, 'La educaci�n m�dica en la Rusia sovi�tica', '001_06_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(6, 'Programas intramurales y en servicio de educaci�n de postgraduados. El papel de los hospitales y de las escuelas de medicina.', '001_06_11.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(6, 'Los objetivos y problemas de la continuaci�n de la Educaci�n m�dica.', '001_06_12.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(6, 'Objetivos y problemas de la continuaci�n de la Educaci�n m�dica.', '001_06_13.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, '', '001_07_18.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, '', '001_07_19.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'Editorial', '001_07_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'El prestigio de las profesiones.', '001_07_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'La universidad y la formaci�n profesional.-', '001_07_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'La misi�n del m�dico en la sociedad.', '001_07_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'Universidades y educaci�n m�dica', '001_07_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'Conceptos sobre ense�anza m�dica', '001_07_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'Orientaci�n para la formaci�n del profesional m�dico', '001_07_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'Los prop�sitos de la medicina en nuestra sociedad. La misi�n del m�dico y su formaci�n profesional.', '001_07_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'Los prop�sitos de la medicina en nuestra sociedad. La misi�n del m�dico y su formaci�n profesional.', '001_07_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'Sobre ense�anza de la medicina', '001_07_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'Medicina, ciencia, libertad y responsabilidad.', '001_07_11.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'Prop�sitos de la medicina contempor�nea en Chile', '001_07_12.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'La formaci�n del m�dico', '001_07_13.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'Prop�sitos de la medicina en cuentra sociedad. la misi�n del m�dico y su formaci�n profesional.', '001_07_14.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'Problemas en Educaci�n m�dica', '001_07_15.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'Problemas de la Educaci�n m�dica chilena', '001_07_16.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(7, 'Rol del m�dico pr�ctico en los programas de protecci�n de la salud.', '001_07_17.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 1 AND PublishYear = 1960)),
(1, '', '002_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(1, '', '002_01_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(1, 'Editorial', '002_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(1, 'Opinan sobre el seminario de formacion profesional medica', '002_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(1, 'S�ntesis bibliogr�fica sobre la educaci�n de graduados. 2� Conferencia Mundial de la Educaci�n m�dica. chicago, 1959', '002_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(1, 'Sociolog�a m�dica. Hospitalizaci�n en el �rea Metropolitana del Gran santiago', '002_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(1, 'Estudiantes de medicina. En torno al seminario de formaci�n profesional m�dica.', '002_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(2, '', '002_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(2, '', '002_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(2, '', '002_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(2, 'Organizaci�n de la atenci�n m�dica. El consultorio externo: comentarios y conclusiones de dos mesas redondas.', '002_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(2, 'Organizaci�n de la atenci�n m�dica. Una experiencia en medicina integral.', '002_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(2, 'Organizaci�n de la atenci�n m�dica. Informe sobre Chile, sus servicios de salud y la educaci�n m�dica.', '002_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(2, 'Demograf�a y salud. Encuesta de fecundidad y de actitudes relativas a la formaci�n de la familia.', '002_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(2, 'El aborto provocado: s�ntesis bibliogr�fica reciente.', '002_02_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(2, 'Educaci�n medica. El m�dico general en el Servicio Nacional de Salud', '002_02_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(2, 'Estudiantes de de Medicina. Una nueva l�nea de trabajo de los estudiantes de medicina.', '002_02_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 2 AND PublishYear = 1961)),
(1, 'Editorial', '003_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(1, 'Organizaci�n de la Atenci�n m�dica. Sobre consultorio externo de adultos.', '003_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(1, 'Organizaci�n de la Atenci�n m�dica. Integraci�n del problema de salud mental a los programas de salud.', '003_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(1, 'Seguridad Social. �Qu� es la seguridad social?', '003_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(1, 'Seguridad Social. Los m�dicos y la seguridad social en Chile.', '003_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(1, 'Educaci�n m�dica. La necesidades de m�dicos en Chile', '003_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(1, 'Educaci�n m�dica. Ense�anza de la pediatr�a en terreno.', '003_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(1, 'Desarrollo econ�mico y salud. Programaci�n de salud y desarrollo econ�mico (Primera parte)', '003_01_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(1, 'Desarrollo econ�mico y salud. La Sociedad Chilena de Planificaci�n y desarrollo (Plandes). Sus prop�sitos y objetivos.', '003_01_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(2, 'Editorial', '003_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(2, 'Organizaci�n de la Atenci�n m�dica. �Cu�les son los beneficios del Servicio Nacional de Salud?', '003_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(2, 'Desarrollo econ�mico y salud. Programaci�n de salud y desarrollo econ�mico (Segunda parte)', '003_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(2, '"Desarrollo econ�mico y salud. Primer seminario viajero sobre ""Organizaci�n de Escuelas Latinoamericanas de Medicina"""', '003_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(2, 'Educaci�n M�dica. �Qu� tipo de m�dico necesita el pais?', '003_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(2, '"Educaci�n M�dica. Primer seminario viajero sobre ""Organizaci�n de Escuelas Latinoamericanas de Medicina"""', '003_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(2, 'Demograf�a y salud. El aumento de poblaci�n en el mundo y los problemas que plantea.', '003_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 3 AND PublishYear = 1962)),
(2, '', '004_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1962)),
(2, '', '004_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1962)),
(2, 'editorial', '004_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1962)),
(2, 'Educaci�n m�dica. �Qu� tipo d em�dico necesita el pa�s?', '004_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1962)),
(2, 'Educaci�n m�dica. Lo social en la ense�anza m�dica en Concepci�n-Chile', '004_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1962)),
(2, 'Sociologia m�dica. Sociologia y medicina. Bases de las relaciones medico-paciente', '004_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1962)),
(2, 'Demograf�a y salud. Tendencias recientes de la mortalidad en Chile.', '004_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1962)),
(2, 'Ecologia y salud. El hombre, la ecolog�a y el control de la enfermedad.', '004_02_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1962)),
(3, '', '004_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(3, '', '004_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(3, 'Educaci�n m�dica', '004_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(3, 'Politica Universitaria. La universidad en el mundo entero.', '004_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(3, 'Pensamiento de los decanos de facultades de medicina. Problemas de la educaci�n, Formaci�n y Ejercicio Profesionales.', '004_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(3, 'Pensamiento de los decanos de facultades de medicina. Problemas de la educaci�n, Formaci�n y Ejercicio profesionales.', '004_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(3, 'Pensamiento de los decanos de facultades de medicina. Problemas fundamentales de la escuela de medicina', '004_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(3, 'Pensamiento de los decanos de facultades de medicina. Formaci�n profesional en las Escuelas de Medicina', '004_03_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(3, 'Comit� Formaci�n Profesional M�dica. Informe de actividades del comit�.', '004_03_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(3, 'Comit� Formaci�n Profesional M�dica. Necesidades de m�dicos en Chile', '004_03_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(3, 'Comit� Formaci�n Profesional M�dica. Encuesta a Escuelas profesionales de Colaboraci�n M�dica', '004_03_11.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(3, 'Reuniones Internacionales. El m�dico y la prevenci�n.', '004_03_12.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(4, '', '004_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(4, '', '004_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(4, 'Educaci�n m�dica', '004_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(4, 'Educaci�n M�dica. Informe sobre Educaci�n M�dica en la rep�blica Federal Alemana, Inglaterra, Francia, Italia e Israel.', '004_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(4, 'Ejercicio Profesional en provincias. Una experiencia de medicina rural en Frutillar, provincia de Llanquihue.', '004_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(4, 'Reuniones Internacionales. Educaci�n m�dica en la Uni�n Sovi�tica.', '004_04_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(4, 'Atenci�n M�dica. Los m�dicos domo recursos de salud', '004_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(4, 'Problemas de Ense�anza. La ense�anza m�dica vista por el estudiante.', '004_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 4 AND PublishYear = 1963)),
(1, '', '005_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(1, 'Sociologia y medicina.', '005_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(1, 'Sociologia m�dica. La Sociolog�a de la Medicina.', '005_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(1, 'Sociolog�a y elites m�dicas. Comportamiento de las elites m�dicas en una situaci�n de subdesarrollo.', '005_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(1, 'Sociolog�a de la organizaci�n. La medicina y los problemas de organizaci�n.', '005_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(1, 'Formaci�n profesional m�dica y sociolog�a. Investigaci�n sobre motivaciones, valores y rol m�dico joven.', '005_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(2, '', '005_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(2, '', '005_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(2, 'Demograf�a y salud. Demograf�a en la Am�rica Latina y situaci�n actual en Chile.', '005_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(2, 'Desarrollo econ�mico y salud. la salud en funci�n del desarrollo econ�mico en Am�rica Latina.', '005_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(2, 'Planificaci�n y salud. Planificaci�n en salud. Conceptos generales.', '005_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(2, 'Planificaci�n y salud. Nuevas tendencias en la planificaci�n de la salud.', '005_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(4, '', '005_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(4, '', '005_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(4, 'Legislaci�n y servicios de salud. Apreciaciones sobre legislacon de salud p�blica. Intervenci�n del abogado en un servicio de salud.', '005_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(4, 'Psicolog�a social y salud. Factores socio-culturales en la desnutrici�n del lactante.', '005_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 5 AND PublishYear = 1964)),
(16, 'Comunidad y salud.Organizaci�n y desarrollo de la comunidad vistos por un sanitario.', 'falta de la 13_16', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 0 AND PublishYear = 1964)),
(1, '', '006_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(1, '', '006_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(1, 'Salud P�blica y Administraci�n. Medicina p�blica y privada. Las cr�ticas al Servicio Nacional de Salud.', '006_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(1, 'Salud Mental. Integraci�n y comunicaci�n en el campo desalud mental. Una experiencia brit�nica.', '006_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(1, 'Educaci�n m�dica. El trabajo cient�fico del estudiante de medicina', '006_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(1, 'El personal de colaboraci�n m�dica y la organizaci�n. El personal param�dico.', '006_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(2, '', '006_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(2, '', '006_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(2, 'Conferencia latinoamericana sobre agricultura y alimentaci�n. La agricultura y al revoluci�n econ�mica mundial.', '006_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(2, 'Conferencia latinoamericana sobre agricultura y alimentaci�n. Proyecto de informe-pre�mbulo. Pol�ticas alimentarias y desarrollo econ�mico. Pol�ticas de reforma agraria.', '006_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(2, 'Nutrici�n y salud. Malnutrici�n, problema nacional.', '006_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(2, 'Nutrici�n y salud. La disponibilidad de alimentos en Am�rica Latina.', '006_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(3, '', '006_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(3, '', '006_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(3, 'Declaraciones del Director del S. N. S. El Servicio Nacional de Salud visto por el nuevo Director General.', '006_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(3, 'Sociolog�a m�dica. Formaci�n m�dica y pr�ctica profesional.', '006_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(3, 'Educaci�n m�dica y sociolog�a. Las escuelas de medicina y el soci�logo.', '006_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(3, 'Educaci�n M�dica. Formaci�n preventiva y social del estudiante de cl�nica.', '006_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(3, 'Planificaci�n y salud. Formulaci�n de un Plan Nacional de Salud para Chile.', '006_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(3, 'Enfermer�a y salud. Nuestro prop�sito: Mejorar la atenci�n de enfermer�a.', '006_03_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(4, '', '006_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(4, '', '006_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(4, 'Cambio social y salud mental. La salud mental en la vida social contempor�nea.', '006_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(4, 'Servicios m�dicos y su financiamiento. La provisi�n de fondos pala los Servicios M�dicoa en los paises de latinoam�rica.', '006_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(4, 'Sociolog�a y salud. La Salud P�blica y las Ciencias Sociales.', '006_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 6 AND PublishYear = 1965)),
(1, '', '007_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(1, '', '007_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(1, '', '007_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(1, '', '007_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(1, 'Nutrici�n y salud. La alimentaci�n en Latinoam�rica (Prediagn�stico del problema)', '007_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(1, 'Econom�a y salud. Plan nacional de desarrollo econ�mico y social, 1966-1970...', '007_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(1, 'Planificaci�n y salud. Fundamentos y caracter�sticas b�sicas del Plan Nacional de salud de Chile.', '007_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(1, 'Planificaci�n y salud. Problemas conceptuales en la Planificaci�n en Salud.', '007_01_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(1, 'Planificai�n y salud. La Planificaci�n de la atenci�n m�dica.', '007_01_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(2, '', '007_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(2, '', '007_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(2, '', '007_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(2, 'Dia mundial de la salud. La salud del hombre de la gran ciudad. Simposium Sociedad Chilena de salubridad, Facultad de Medicina Universidad de Chile y Servicio Nacional de Salud.', '007_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(2, 'Demograf�a y salud. Regulaci�n de la Natalidad en el Servicio Nacional de Salud de Chile.', '007_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(2, 'Educaci�n m�dica. Seminario de Medicina Preventiva en las cl�nicas (Una experiencia docente)', '007_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(2, 'Educaci�n m�dica. Una experiencia vivida como estudiantes de medicina.', '007_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(2, 'Educaci�n secundaria y salud. Desarrollo del adolescente (Encuesta de conocimientos entre maestros del area central de la V zona de salud)', '007_02_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(2, 'Correspondencia', '007_02_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(3, '', '007_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(3, '', '007_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(3, '', '007_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(3, '', '007_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(3, 'Psiquiatr�a. Relaci�n de la Psiquiatr�a con el resto de las disciplinas m�dicas.', '007_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(3, 'IX Congreso de Medicina Social. Rese�a de los trabajos chilenos presentados en el IX Congreso M�dico-social de la Confederaci�n M�dica Panamericana.', '007_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(3, 'Educaci�n M�dica. Requisitos m�nimos en la Planeaci�n de nuevas Escuelas de Medicina.', '007_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(3, 'Educaci�n M�dica. Programa de Ense�anza de Ciencias Sociales en las Escuelas de Medicina de Am�rica Latina.', '007_03_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(3, 'Educaci�n M�dica. Lo social y lo integral en la Asistencia M�dica y en la Ense�anza de la Medicina.', '007_03_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(3, 'Correspondencia', '007_03_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(4, '', '007_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(4, '', '007_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(4, '', '007_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(4, '', '007_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(4, 'Sociolog�a M�dica. Formaci�n profesional.', '007_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(4, 'Sociolog�a M�dica. Aporte de las Ciencias Sociales la formaci�n m�dica en el nivel pre-graduado.', '007_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(4, 'Sociolog�a Econ�mica y salud. El gasto m�dico familiar.', '007_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(4, 'Socilog�a y Adolescencia. Investigaciones sobre adolescencia.', '007_04_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 7 AND PublishYear = 1966)),
(1, '', '008_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(1, '', '008_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(1, '', '008_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(1, '', '008_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(1, 'Educaci�n m�dica. Indicadores de educaci�n m�dica en latinoam�rica. (Un estudio centrado en la Ense�anza Preventiva Social)', '008_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(1, 'Sociolog�a Econ�mica y salud. El gasto m�dico del sector p�blico en Chile (Estudio hasta 1965 inclusive)', '008_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(1, 'Administraci�n en salud. La racionalizaci�n administrativa en el Servicio Nacional de Salud.', '008_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(1, 'Ejercicio Profesional en provincias. Experiencia de un m�dico general de Zona en Achao. (Octubre 1964 - Mayo 1967)', '008_01_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(2, '', '008_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(2, '', '008_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(2, '', '008_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(2, '', '008_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(2, 'Planificaci�n y salud. Ensayo de una Tipolog�a de Salud.', '008_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(2, 'Rol profesional. Roles profesionales en el equipo de atenci�n de la salud.', '008_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(2, 'Educaci�n m�dica. La ense�anza M�dica Integrada en la nueva Escuela de Medicina de Valpara�so, Chile.', '008_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(2, 'Ejercicio Profesional en provincias. Experiencia de un m�dico general de zona en Los Vilos. 1964-1966.', '008_02_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(2, 'Formaci�n Profesional. II Seminario de Formaci�n Profesional M�dico.', '008_02_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(3, '', '008_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(3, '', '008_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(3, '', '008_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(3, '', '008_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(3, 'Problemas de atenci�n m�dica. Producci�n y distribuci�n de m�dicos en America Latina en relaci�n con las necesidades de salud y el aumento de poblaci�n.', '008_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(3, 'Nutrici�n infantil y salud. Nutrici�n infantil y programas del Servicio Nacional de Salud', '008_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(3, 'Demografia y salud. Chile: Tablas abreviadas de mortalidad por regiones. 1960-1961', '008_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(3, 'Ejercicio Profesional en provincias. Experiencia de un m�dico general de zona en Vicu�a. 1963-1967.', '008_03_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(3, 'Formaci�n Profesional M�dica. Las conclusiones y recomendaciones del Primer Seminario de Formaci�n Profesional. La tarea del segundo seminario', '008_03_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(3, 'Formaci�n Profesional M�dica. Problemas de medicina integral: distintos enfoques.', '008_03_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(3, 'Educaci�n M�dica en paises en desarrollo', '008_03_11.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(4, '', '008_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(4, '', '008_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(4, '', '008_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(4, '', '008_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(4, 'Ciencias sociales m�dicas. la integraci�n de las ciencias medicas y sociales como base de la innovaci�n', '008_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(4, 'Ejercicio Profesional en provincias. Experiencia de un m�dico general de zona en Tiltil 1964-1967', '008_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(4, 'Ejercicio Profesional en provincias. Una experiencia de medicina rural en Frutillar, provincia de Llanquihue', '008_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(4, 'Formaci�n Profesional M�dica. Informe de la Secretar�a Ejecutiva', '008_04_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(4, 'Formaci�n Profesional M�dica. La ense�anza de las ciencias sociales en las escuelas de medicina', '008_04_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(4, 'El rol del departamento de medicina social en al educacion m�dica de pregrado', '008_04_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(4, 'De nuestros lectores. Nutrici�n infantil y programas del S. N-S.', '008_04_11.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 8 AND PublishYear = 1967)),
(1, '', '009_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(1, '', '009_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(1, '', '009_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(1, '', '009_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(1, 'Demografia y salud. La ense�anza de la demograf�a y su influencia en medicina y salubridad.', '009_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(1, 'Ciencias sociales y salud. Una necesidad emergente. Investigaciones sociales relacionadas con la salud P�blica. Problemas y perspectivas.', '009_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(1, 'Ciencias sociales y salud. El rol de la investigaci�n en ciencias sociales en los recientes programas de salud en America Latina.', '009_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(1, 'Pol�tica de salud. Una nueva politica social para la salud.', '009_01_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(1, 'Formaci�n profesional m�dico. Cuarto informe de la secretar�a ejecutiva del II Seminario de formacion profesional m�dica', '009_01_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(1, 'Formaci�n profesional m�dico. Extracto de una carta de un m�dico general de zona a un ex profesor.', '009_01_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(2, '', '009_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(2, '', '009_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(2, '', '009_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(2, '', '009_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(2, 'Ejercicio profesional. El ejercicio de la medicina en provincia (estudio de la promoci�n de m�dicos de 1959)', '009_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(2, 'Administraci�n y salud. La planificaci�n y la administraci�n en salud p�blica.', '009_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(2, 'Sociologia y burocracia. Modernismo, satisfacc�n y radicalismo entre los funcionarios de un Servicio Nacional de Salud.', '009_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(2, 'Formaci�n profesional m�dico. Informe sobre actividades del segundo seminario de formacion medica', '009_02_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(2, 'Formac�n profesional m�dico. Nuevo programa de ense�anza de la semiolog�a en la escuela de medicina de la Universidad de Concepcion.', '009_02_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(3, '', '009_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(3, '', '009_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(3, '', '009_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(3, '', '009_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(3, 'Educaci�n m�dica y reforma universitaria. Reforma Universitaria, problemas de salud y atenci�n m�dica en Chile.', '009_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(3, 'Educaci�n m�dica y reforma universitaria. Cambios en la educaci�n m�dica en la Universidad Cat�lica.', '009_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(3, 'Educaci�n m�dica y reforma universitaria. Reforma Universitaria Y Educaci�n M�dica.', '009_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(3, 'Problemas de la atenci�n m�dica. Algunas caracteristicas de los recursos humanos medicos en chile', '009_03_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(3, 'Formaci�n profesional m�dico. Elecci�n de una especialidad m�dica', '009_03_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(3, 'Demografia y salud. Justificaci�n de la Ense�anza de Demograf�a.', '009_03_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(4, '', '009_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(4, '', '009_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(4, '', '009_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(4, '', '009_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(4, 'Nutrici�n infantil y salud. Efecto de la nutricion y medio ambiente sobre el desarrollo psico-motor en el ni�o.', '009_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(4, 'Ciencias Sociales y salud. Investigaci�n intramericana de mortalidad en la ni�ez.', '009_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(4, 'Salud y economia. la contribuci�n de los servicios del salud al crecimiento economico y al bienestar.', '009_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(4, 'Sociologia y burocracia. Trabajo, iniciativa e insatisfaccion en un hospital del servicio Nacional de Salud.', '009_04_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(4, 'Formaci�n profesional m�dica. El rol de la medicina en los paises en desarrollo.', '009_04_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 9 AND PublishYear = 1968)),
(1, '', '010_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(1, '', '010_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(1, '', '010_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(1, '', '010_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(1, 'Demografia y planificacion de la familia. El crecimiento de la poblacion y el control de la natalidad.', '010_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(1, 'Planificaci�n de la familia y salud. Efectos de un programa de planificai�n de la familia sobre las tasas de fecundidad y aborto de una poblacion marginal de Santiago.', '010_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(1, 'Ejercicio Profesional en provincias. Experiencia de un medico general de zona Vallenar', '010_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(1, 'Secci�n bibliogr�fica. sociologia y el campo de la Salud P�blica.', '010_01_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(1, 'Formaci�n Profesional M�dica. El medio institucional de la asistencia m�dica en Chile.', '010_01_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(2, '', '010_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(2, '', '010_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(2, '', '010_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(2, '', '010_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(2, 'Educaci�n en planificacion de la familia. Los hombres y la educaci�n en planificaci�n de la familia.', '010_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(2, 'Atenci�n m�dica. El impacto de la atencion m�dica estatal en una ciudad de Canad�, 1960-1965', '010_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(2, 'Educaci�n y nutricion infantil. la educaci�n materna en la desnutrici�n infantil de un �rea rural.', '010_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(2, 'Ejercicio Profesional en provincias. Experiencia de un medico general de zona en Puerto Octay', '010_02_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(2, 'Secci�n bibliogr�fica. La investigaci�n de una muestra magistal en Washington Heights.', '010_02_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(2, 'Formaci�n Profesional M�dica. Formaci�n del medico en Chile.', '010_02_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(3, '', '010_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(3, '', '010_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(3, '', '010_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(3, '', '010_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(3, 'Mortalidad infantil y atenci�n m�dica. Estudio de defunciones infantiles sin certificacion medica en el Area Hospitalaria Norte de santiago, 1966-1967', '010_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(3, 'Planificaci�n de la Familia. Los hombres, la anticoncepci�n y el aborto.', '010_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(3, 'Econom�a y salud. Proyecciones y estimaciones del gasto medico del sactor publico.', '010_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(3, 'Ejercicio Profesional en provincias. Experiencias de un m�dico general de zona en Santa Juana, Concepci�n.', '010_03_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(3, 'Instituto de Neurocirug�a. El Instituto de Neurocirugia e investigaciones cerebrales de Chile celebra se Trig�simo aniversario', '010_03_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(3, 'Equipos de salud. Premios a equipos de salud en medios rurales.', '010_03_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(3, 'Formaci�n Profesional M�dica. Conclusiones conjuntas de los grupos de discusi�n sobre el temario propuesto en el segundo seminario de formacion profesional medica.', '010_03_11.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(4, '', '010_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(4, '', '010_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(4, '', '010_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(4, '', '010_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(4, 'Adicci�n a drogas y Salud P�blica. Algunos de la adicci�n a drogas.', '010_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(4, 'Accidentes de tr�nsito y salud p�blica. Los accidentes del tr�nsito como problema de salud p�blica.', '010_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(4, 'Planificaci�n de la Familia. Algunos aspectos del comportamiento sexual masculino.', '010_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(4, 'Atenci�n m�dica. Algunas relaciones de la atenci�n m�dica con el sistema social.', '010_04_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 10 AND PublishYear = 1969)),
(1, '', '011_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(1, '', '011_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(1, '', '011_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(1, '', '011_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(1, 'Ecolog�a. La ecolog�a humana.', '011_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(1, 'Ciencias Sociales. La familia en el mundo contempor�neo.', '011_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(1, 'Nutrici�n Infantil. Desnutricion precoz y desarrollo mental; una relaci�n seriamente mal entendida.', '011_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(1, 'Ciencias Sociales. El profesional en la burocracia: el m�dico en el S. N. S', '011_01_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(1, 'Educaci�n m�dica. Internado rural en Concepci�n.', '011_01_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(2, '', '011_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(2, '', '011_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(2, '', '011_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(2, '', '011_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(2, 'Nutrici�n Infantil. Latinoamerica; desnutrici�n y condiciones de vida.', '011_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(2, 'Demografia y salud. Humanidad devastadora.', '011_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(2, 'Demografia, econom�a y salud. Salud, poblaci�n y desarrollo econ�mico', '011_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(2, 'Ciencias Sociales y medicina. Ciencias Sociales: Una discusi�n acerca de su enfoque en Medicina', '011_02_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(3, '', '011_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(3, '', '011_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(3, '', '011_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(3, '', '011_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(3, 'Mortalidad infantil y nivel de vida. Mortalidad infabtil en Chile: Tendencias recientes.', '011_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(3, 'Ciencias Sociales y salud. Roles profesionales y atenci�n de la salud.', '011_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(3, 'Demografia. Algunas caracteristicas demogr�ficas del gran Santiago.', '011_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(3, 'Salud y Comunidad. La participaci�n comunitaria en salud.', '011_03_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(3, 'Salud y Comunidad. La medicina comunitaria y las v�as de desarrollo socio-econ�mico', '011_03_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(3, 'Medio rural y planificaci�n familiar. Caracter�sticas demogr�ficas y planificaci�n familiar en �reas rurales de Chile', '011_03_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(4, '', '011_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(4, '', '011_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(4, '', '011_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(4, '', '011_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(4, 'Administraci�n y niveles de salud en el S. N. S. Algunos aspectos del Servicio Nacional de Salud en 15 a�os, 1955-1969', '011_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(4, 'Planificaci�n de la salud. Antecedentes sobre planificaci�n en el Servicio Nacional de Salud en Chile.', '011_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(4, 'Sociologia y enfermeria. Estudio de los roles en el equipo de enfermeria.', '011_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(4, 'Nutrici�n y salud. Formaci�n y ejecuci�n de pol�ticas nacionales de alimentacion y nutrici�n.', '011_04_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 11 AND PublishYear = 1970)),
(1, '', '012_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(1, '', '012_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(1, '', '012_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(1, 'Planificaci�n de la familia. Evaluaci�n de los efectos de un programa de planificaci�n familiar sobre la fecundidad en una poblaci�n marginal de Santiago, Chile', '012_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(1, 'Ejercicio Profesional en provincias. Experiencia de un medico general de zona en el hospital de Quell�n, Chilo� (a�os 1966 a 1970)', '012_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(1, 'Plaificacion y salud. Tendencias actuales der la planificacion en el Servicio Nacional de Salud.', '012_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(1, 'Planificacion de la familia. Influencia de algunas caracter�sticas del esposo sobre el uso de anticonceptivos por su mujer. (Estudio de parejas)', '012_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(2, '', '012_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(2, '', '012_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(2, '', '012_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(2, 'V Congreso internacional de AMIEVS. Proteccion de la infancia en URSS', '012_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(2, 'V Congreso internacional de AMIEVS. La infancia en Cuba (Resumido)', '012_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(2, 'V Congreso internacional de AMIEVS. La infancia en Chile.', '012_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(2, 'Formaci�n profesional. La formaci�n profesional de salud de nivel medio en Cuba.', '012_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(3, '', '012_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(3, '', '012_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(3, '', '012_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(3, 'Economia y salud. Hambre, salud y desarrollo.', '012_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(3, 'Nutrici�n y salud. Las levaduras como recurso proteico en la dieta humana; algunos aspectos de su problematica', '012_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(3, 'Docencia y asistencia medica. las relaciones entre Facultades de Medicina y el Servicio Nacional de Salud.', '012_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(3, 'Demografia y salud. America latina, Chile y las pol�ticas de poblaci�n.', '012_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(3, 'Ejercicio Profesional en provincias. Experiencia de equipo de salud del hospital de Coronel', '012_03_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(4, '', '012_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(4, '', '012_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(4, '', '012_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(4, 'Medio ambiente humano. El ambiente, al ecolog�a y la epidemiolog�a.', '012_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(4, 'Natalidad y variables socio-econ�micas. Escolaridad y conducta reproductiva.', '012_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(4, 'Atenci�n m�dica. Programa de direcci�n en Consultorio Distrital', '012_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(4, 'Atenci�n medica. An�lisis y consideraciones sobre consultorio externo del servicio medicina. Hospital Rancagua a�o 1971.', '012_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(4, 'Ejercicio Profesional en provincias. Experiencia del equipo de salud del hospital San Pablo de Coquimbo.', '012_04_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 12 AND PublishYear = 1971)),
(1, '', '013_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(1, '', '013_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(1, '', '013_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(1, 'Sociologia y pr�ctica profesional. El m�dico general de zona: im�genes de su trabajo, valores y ocupaciones y estudio de satisfacci�n profesional.', '013_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(1, 'Ejercicio Profesional en provincias. Experiencia de un m�dico general de zona en Hospital de Nacimiento.', '013_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(1, 'Medio Humano y salud. Primeras jornadas sobre contaminaci�n del ambiente en chile.', '013_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(1, 'Medio Humano y salud. Problemas del medio humano en Chile desde el punto de vista de la salud publica.', '013_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(1, 'Medio Humano y salud. Implicaciones educativas en el problema de contaminaci�n ambiental.', '013_01_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(1, 'Higiene Materna e infantil. Higiene Materna e infantil en Chile.', '013_01_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(2, '', '013_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(2, '', '013_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(2, '', '013_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(2, 'Informes b�sicos de la delegaci�n chilena para la conferencia. Declaraci�n inaugural pronunciada por el se�or Maurica F. Strong, Secretario General de la Conferencia.', '013_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(2, 'Informes b�sicos de la delegaci�n chilena para la conferencia. Participaci�n del sector salud de Chile en la b�squeda de soluciones a los problemas del Medio Humano.', '013_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(2, 'Informes b�sicos de la delegaci�n chilena para la conferencia. Informaci�n sobre el proyecto de Programa Nacional contra la Contaminaci�n Ambiental en Chile.', '013_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(2, 'Informes b�sicos de la delegaci�n chilena para la conferencia. Desastres naturales y catastrofes.', '013_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(2, 'Pre�mbulos y principios de la declaraci�n de Estocolmo. Declaraci�n de la Conferencia de Naciones Unidas sobre el Medio Humano.', '013_02_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(2, 'Ejercicio Profesional en provincias. Experiencia de un medico general de zona en Quemchi, Isla de Chilo�.', '013_02_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(3, '', '013_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(3, '', '013_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(3, '', '013_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(3, 'Medio ambiente humano. Discurso de Shimati Indira Gandhi, primer ministro de la india', '013_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(3, 'Revoluci�n cientifico-t�cnica en la medicina. Centros de alto nivel en salud.', '013_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(3, 'Gastos en salud en Chile. Algunas consideraciones sobre gastos en salud en Chile y su financiamiento.', '013_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(3, 'Recurso m�dico en chile. Distribuci�n del recurso medico, chile S. N. S.', '013_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(4, '', '013_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(4, '', '013_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(4, '', '013_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(4, 'El doctor Salvador D�az', '013_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(4, 'Alimentacion infantil. Programa Nacional de Leche.', '013_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(4, 'Atenci�n medica. Patrones de atencion m�dica de lactantes.', '013_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(4, 'Medio ambiente humano. Consideraciones generales sobre la posicion internacional de Chile en relacion con los problemas del medio humano.', '013_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(4, 'Ejercicio Profesional en provincias. Acciones de salud integral realizadas por el equipo de salud del Hospital de Lonquimay 1969 a 1971.', '013_04_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 13 AND PublishYear = 1972)),
(1, '', '014_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(1, '', '014_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(1, '', '014_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(1, 'Menores en situacion irregular. El menor en situacion irregular en un distrito del gran Santiago. Medici�n de su frecuencia y de la asociaci�n con algunas variables. 1970', '014_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(1, 'Atenci�n medica. Morbilidad de Lactantes.', '014_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(1, 'Ejercicio Profesional en provincias. Experiencia de un medico general de zona. Hospital de Calbuco - XII Zona de Salud.', '014_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(2, '', '014_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(2, '', '014_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(2, '', '014_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(2, 'Administraci�n de servicios de salud. El m�dico en la Administraci�n de los Servicios de Salud', '014_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(2, 'Planificaci�n familiar y aborto inducido. Aborto y planificaci�n familiar.', '014_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(2, 'Mortalidad Infantil Rural. Alimentacion infantil y mortalidad en Chile rural.', '014_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(2, 'Ejercicio Profesional en provincias. Experiencia de un Equipo de salud en Teno, Curic�-1969-1971.', '014_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(3, '', '014_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(3, '', '014_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(3, '', '014_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(3, 'Utilizaci�n de Servicios de Salud. Factores que determinan la falta de respuesta a manifiestas necesidades de utilizar los servicios de Salud en Chile.', '014_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(3, 'Nutrici�n: crecimiento y desarrollo. Estado de salud de la poblaci�n infanto-juvenil del �rea Norte de Santiago, en relaci�n a la condici�n nutricional y de su crecimiento y desarrollo.', '014_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(3, 'Alcoholismo. Alcoholismo: Nociones b�sicas para m�dicos generales.', '014_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(3, 'Problemas de la Infancia y adolescencia. Orientaci�n de la pol�tica en favor de la Infancia en los pa�ses en v�as de desarrollo.', '014_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(4, '', '014_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(4, '', '014_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(4, '', '014_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(4, 'Crecimiento y desarrollo. Comparaci�n de algunos rasgos antropom�tricos entre escolares del �rea Hospitalaria Norte de Santiago y algunas Tablas Internacionales.', '014_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(4, 'Sociologia y nutrici�n infantil. Estudio sobre conocimientos, opiniones y actitudes del equipo de salud materno infantil respecto a la desnutrici�n infantil. Consultorios perif�ricos �rea Hospitalaria Norte de Santiago.', '014_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(4, 'Sociologia y regulaci�n de natalidad. Consecuencias psico-sociales del uso de anticonceptivos.', '014_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(4, 'Salud Ocupacional y Seguridad Social. Seguridad Social y Salud Ocupacional. Rol de las Mutualidades en Chile.', '014_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 14 AND PublishYear = 1973)),
(1, '', '015_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(1, '', '015_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(1, '', '015_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(1, 'Tecnolog�a m�dica y equipo de salud. Recursos de Tecnolog�a M�dica, situaci�n actual y perspectivas, Chile, noviembre 1973.', '015_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(1, 'Recursos humanos y enfermeria. An�lisis de la distribuci�n del recurso enfermera en el servicio de salud (a�os 1970-1971)', '015_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(1, 'Servicios m�dicos y cat�strofes. la atenci�n m�dica en caso de catastrofe', '015_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(1, 'Salud P�blica y el senescente. Bases para una pol�tica del Senescente', '015_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(2, '', '015_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(2, '', '015_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(2, 'El Dr. Horacio Boccardo deja la direcci�n de cuadernos medico-sociales.', '015_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(2, 'Sistemas de informaci�n y salud. Informe sobre uso de sistemas electr�nicos de informaci�n en el Servicio Nacional de Salud.', '015_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(2, 'Epidemiolog�a del c�ncer. Algunos aspectos epidemiol�gicos de la mortalidad por c�ncer en Chile.', '015_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(2, 'Alcoholismo y salud mental. El alcoholismo como objetivo asistencial del Servicio Nacional de Salud', '015_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(2, 'Alcoholismo. Aspectos socio�con�micos. Los problemas del alcohol y alcoholismo y la econom�a nacional.', '015_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(2, 'Sociolog�a y enfermedades cr�nicas. Aspectos psico-sociales de las enfermedades del coraz�n.', '015_02_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(2, 'Medicina del trabajo y cardiopat�as. La cardiopatia coronaria, la legislaci�n y el esfuerzo fisico como problemas sociales.', '015_02_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(3, '', '015_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(3, '', '015_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(3, '', '015_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(3, 'Orientaci�n human�stica y carrera de medicina. Estudio comparativo del puntaje de la Prueba de Aptitud Acad�mica y las notas de bioestadistica, matematicas 101 y matem�ticas 102. Primer a�o de medicina, Universidad Cat�lica. A�os 1972-1973-1974.', '015_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(3, 'Nutricion y salud. Problemas nutricionales de Chile. Diagn�stico de la situaci�n.', '015_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(3, 'Ayuda internacional y catastrofes. La ayuda internacional en casos de desastres naturales.', '015_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(3, 'Recursos humanos y enfermer�a. Recursos de enfermeras del Servicio Nacional de Salud. Diciembre 1973.', '015_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(4, '', '015_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(4, '', '015_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(4, '', '015_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(4, 'Lactancia materna. Duraci�n de la lactancia materna y algunos factores condicionantes.', '015_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(4, 'Ejercicio profesional en provincia. Panguipulli. Tres a�os de labor en salud.', '015_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(4, 'Epidemiologia en la tuberculosis. Epidemiologia en la tuberculosis en los Hospitales tipo C y D. XII zona de salud, Puerto Montt. Quinquenio 1967-1971.', '015_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(4, 'Informaci�n patologia bucal. Centro de referencia e patologia bucal para Latinoam�rica (Inserci�n solicitada)', '015_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 15 AND PublishYear = 1974)),
(1, '', '016_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(1, '', '016_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(1, '', '016_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(1, 'Lactancia materna. Fomento de la Lactancia materna en el medio escolar a traves de una unidad didactica.', '016_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(1, 'Problema asistencial del quemado. Bases clinicas y epidemiologicas para un pron�stico del enfermo quemado.', '016_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(1, 'Ejercicio profesional en provincia. Experiencia de un equipo de salud en Rosario Lo Solis, Colchagua.', '016_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(1, 'Enfermedades ven�reas y Salud P�blica. Bases para un programa regional de control de las enfermedades ven�reas.', '016_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(2, '', '016_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(2, '', '016_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(2, 'Antropometr�a y Salud. Antropometria en escolares chilenos del �rea Norte de Santiago.', '016_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(2, 'Control prenatal. Evaluaci�n del control prenatal en el �rea Sur de Santiago (1972)', '016_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(2, 'Morbilidad perinatal y ciencias sociales. Factores sociales de la Morbilidad preinatal.', '016_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(2, 'Estad�sticas en consultorios perif�ricos. Estructura de morbilidad de consulta en medicina interna en consultorios perif�ricos urbanos, �rea Occidente de Santiago.', '016_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(3, '', '016_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(3, '', '016_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(3, '', '016_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(3, '', '016_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(3, 'Nuestra poblaci�n m�dica. Algunas caracter�sticas de la poblaci�n m�dica chilena . Primera parte.', '016_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(3, 'Antropometria y salud. Antropometria de escolares fiscales del �rea Metropolitana Norte de Santiago, Segunda parte.', '016_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(3, 'Salud y medio humano. Las naciones Unidas y el medio Humano.', '016_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(3, 'Alcoholismo y comunidad Escolar. Programa de prevenci�n primaria del alcoholismo en la comunidad escolar.', '016_03_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(4, '', '016_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(4, '', '016_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(4, '', '016_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(4, 'La Dra Nathalie P. Masse (fallecimiento)', '016_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(4, 'Nuestra poblaci�n m�dica. Algunas caracter�sticas de la poblaci�n m�dica chilena . Segunda parte.', '016_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(4, 'Antropometr�a y salud. Antropometria de escolares fiscales del Area Metropolitana Norte de Santiago, Tercera parte.', '016_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(4, 'Salud y medio humano. Contaminaci�n de Recursos H�dricos. Contaminaci�n del agua en Chile', '016_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(4, 'Mortalidad y Estado Nutricional. Edad mediana de mortalidad: Indicador del estado nutricional de la comunidad.', '016_04_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(4, 'Alcoholismo y comunidad Escolar. Programa de prevenci�n primaria del alcoholismo en la comunidad escolar.', '016_04_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 16 AND PublishYear = 1975)),
(1, '', '017_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(1, '', '017_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(1, '', '017_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(1, 'Ex�menes de salud. Evaluaci�n del rendimiento de los ex�menes de salud en el Servicio M�dico Nacional de Empleados.', '017_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(1, 'Clasificaci�n social y estado nutritivo. Empleo de un nuevo m�todo de clasificaci�n social.', '017_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(1, 'Lactancia Materna. Contenidos educativos para la promoci�n de la lactancia natural.', '017_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(1, 'Salud y medio humano. Tendencia de la calidad del agua potable en Chile durante el per�odo 1971-1974.', '017_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(1, 'Salud y medio humano. Relaci�n entre calidad del agua potable y morbilidad infecciosa ent�rica: An�lisis de dos situaciones.', '017_01_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(1, 'Ejercicio Profesional en provincias. Labor realizada por el equipo de salud del hospital de Peumo. 1972-1975', '017_01_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(2, '', '017_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(2, '', '017_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(2, '', '017_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(2, 'Control de salud y mortalidad infantil. Nivel de salud y atenci�n pedi�trica preventiva. Una aplicaci�n de ingenier�a de sistemas', '017_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(2, 'Trabajos en turnos y salud laboral. Estudio acerca del sistema de trabajo en turnos rotativos y sus repercusiones en la vida y salud del trabajador.', '017_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(2, 'Salud y medio humano. El problema de la contaminaci�n del aire en Chile.', '017_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(2, 'Ex�menes de salud. Evaluaci�n del rendimiento de los ex�menes de salud en el Servicio M�dico Nacional de Empleados.', '017_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(2, 'Salud Infantil. Aspectos sociol�gicos de la Salud Infantil.', '017_02_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(2, 'Libros.', '017_02_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(3, '', '017_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(3, '', '017_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(3, 'Protecci�n del reci�n nacido. Los problemas de salud del reci�n nacido en Chile.', '017_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(3, 'Nuestra poblacion m�dica. Algunas caracter�sticas de los m�dicos chilenos en el extranjero.', '017_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(3, 'Salud y medio humano. Los desechos solidos y los problemas de su manejo.', '017_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(3, 'C�ncer y factores asociados. Factores asociados al desarrollo del c�ncer c�rvico uterino.', '017_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(3, 'Control de salud y mortalidad infantil. Nivel de salud y atencion pedi�trica preventiva. Una aplicaci�n de ingenier�a de sistemas. Segunda parte', '017_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(3, 'Publicaciones Conferencias. Ciencia social y medicina.', '017_03_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(4, '', '017_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(4, '', '017_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(4, '', '017_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(4, 'Formaci�n Profesional m�dica. Los m�dicos chilenos y las necesidades de formaci�n. Informaci�n b�sica y anotaciones sobre el tema.', '017_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(4, 'Indicadores de nivel de vida y salud. Nivel de vida y salud. An�lisis por regiones y concepto de pobreza extrema.', '017_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(4, 'Salud y medio humano. Programa nacional de Fluoruraci�n del agua potable en Chile.', '017_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(4, 'Adolescencia. El pediatra general y la adolescencia.', '017_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(4, 'Control de salud y mortalidad infantil. Nivel de salud y atenci�n pedi�trica preventiva. Una aplicaci�n de ingenier�a de sistemas. Tercera parte', '017_04_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 17 AND PublishYear = 1976)),
(1, '', '018_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(1, '', '018_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(1, '', '018_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(1, 'Rehabilitaci�n de enfermos mentales. Una experiencia en al rehabilitaci�n de enfermos mentales. Primera parte. Rehabilitaci�n.', '018_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(1, 'Vigilancia epidemiol�gica de la influenza.Influenza A/Victoria /3/75 (H3N2) en Chile. Vigilancia Epidemiol�gica', '018_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(1, 'Vigilancia epidemiol�gica de la influenza.Influenza A/Victoria /3/75 (H3N2) en Chile. Estudio virol�gico.', '018_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(1, 'Salud y medio humano. Asentamientos humanos. Conferencia de Vancouver.', '018_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(2, '', '018_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(2, '', '018_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(2, '', '018_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(2, '', '018_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(2, 'Salud Mental en el Servicio Nacional de Salud.', '018_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(2, 'Psiquiatr�a y salud p�blica. Una experiencia en la rehabilitaci�n de enfermos mentales. Parte II: Evaluaci�n.', '018_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(2, 'Psiquiatr�a y salud p�blica. Estudio de la neurosis en la ciudad de Andacollo.', '018_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(2, '', '018_02_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(2, 'Salud y medio humano. La salud ocupacional en Chile', '018_02_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(2, '', '018_02_11.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(2, 'Educaci�n sanitaria y zoonosis. Apoyo de educaci�n sanitaria a programas de rabia.', '018_02_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(3, '', '018_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(3, '', '018_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(3, '', '018_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(3, '', '018_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(3, 'Aspectos psicol�gicos en la terapia del c�ncer. Algunas consideraciones psicol�gicas en el tratamiento del c�ncer de mama.', '018_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(3, 'Salud materno infantil. Falta de control prenatal estudio de casualidad.', '018_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(3, 'Atenci�n de salud de los adolescentes. Algunas consideraciones sobre formaci�n de profesionales para la atenci�n de la salud de los adolescentes.', '018_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(3, 'Formaci�n profesional m�dica. El internado rural como experiencia docente en la facultad de Medicina sede Santiago Norte.', '018_03_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(3, '"Servicio de Fotocopias. Sistema de informaci�n pedi�trica permanente. Biblioteca Hospital ""Roberto del R�o"""', '018_03_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(4, '', '018_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(4, '', '018_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(4, '', '018_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(4, '', '018_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(4, 'Mortalidad materna y perinatal. Evoluci�n y caracter�sticas de la mortalidad materna y perinatal en Chile.', '018_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(4, 'Anticoncepci�n. An�lisis cr�tico de la anticoncepci�n moderna en Chile.', '018_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(4, 'Nuestra poblaci�n m�dica. Algunas caracter�sticas de la poblaci�n m�dica chilena.', '018_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(4, 'Salud y medio humano. Consideraciones sobre el control de la contaminaci�n ambiental.', '018_04_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 18 AND PublishYear = 1977)),
(1, '', '019_01_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(1, '', '019_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(1, '', '019_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(1, '', '019_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(1, 'El aborto en Chile. Caracter�sticas del aborto en Chile.', '019_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(1, 'Salud Ocupacinal. Estudio descriptivo del trabajo de los choferes de buses interurbanos.', '019_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(1, 'Salud y medio humano. Higiene y control de alimentos.', '019_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(1, 'Ejercicio profesional en provincia. Atenci�n integral del adulto cr�nico en un Hospital perif�rico.', '019_01_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(1, 'Informaciones.', '019_01_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(2, '', '019_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(2, '', '019_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(2, 'Nivel de salud. El uso de encuentas en la medici�n del nivel de salud.', '019_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(2, 'La coordinaci�n docente-asistencial y el mejoramiento de la salud.', '019_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(2, 'Salud y medio humano. Decenio internacional del abastecimiento de agua potable y del saneamiento (1981-1990)', '019_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(2, 'Salud Ocupacional. Estudio descriptivo del trabajo de los choferes de buses interurbanos.', '019_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(3, '', '019_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(3, '', '019_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(3, '', '019_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(3, '', '019_03_04pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(3, 'Econom�a y salud. Econom�a y salud.', '019_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(3, 'Crecimiento y desarrollo del escolar. Algunos rasgos de crecimiento y desarrollo del escolar fiscal del �rea hospitalaria norte de Santiago en comparaci�n con estudios europeos.', '019_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(3, 'Salud y medio humano. Epidemiolog�a de los accidentes de tr�nsito.', '019_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(3, 'Formaci�n profesional. Una gu�a para evaluar programas de formaci�n en psiquiatr�a.', '019_03_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(3, 'Folklore religioso y salud mental. La neurosis de los danzantes de la fiesta religiosa de Andacollo.', '019_03_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(3, 'Desnutrici�n Infantil. H�bito de beber de los pares y la desnutrici�n infantil. Proyecto de investigaci�n.', '019_03_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(3, 'Integraci�n docente asistencial. Caracter�sticas de salud, demogr�ficas y sociales de la poblaci�n beneficiaria del Consultorio de La Pincoya.', '019_03_11.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(4, '', '019_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(4, '', '019_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(4, '', '019_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(4, '', '019_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(4, 'Niveles de salud. Heterogeneidad de la situaci�n de salud en Chile.', '019_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(4, 'Desnutrici�n infantil. Algunos aspectos psicol�gicos maternos de la desnutrici�n infantil.', '019_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(4, 'Formaci�n profesional. Evaluaci�n del programa de m�dicos generales urbanos en el �rea Metropolitana Norte.', '019_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(4, 'Salud y medio humano. Desastres naturales.', '019_04_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(4, 'Salud y medio humano. Plan nacional de emergencia y el sector salud.', '019_04_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 19 AND PublishYear = 1978)),
(4, '', '020_04_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(4, '', '020_04_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(4, '', '020_04_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(4, '', '020_04_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(4, 'Puntajes en las Escuelas de Medicina. Estudio anal�tico de los puntajes de ingreso y puntajes de la carrera en las Facultades de Medicina de Chile.', '020_04_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(4, 'Atenci�n m�dica en gran Santiago. Morbilidad y atenci�n m�dica en el gran Santiago.', '020_04_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(4, 'Salud y medio humano. Los residuos s�lidos, el medio y la salud.', '020_04_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(4, 'Desnutrici�n infantil. Evaluaci�n del uso de alimentos complementarios en desnutridos en el �rea de Coronel.', '020_04_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(1, '', '020_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(1, '', '020_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(1, 'Aborto. An�lisis y evoluci�n de los egresos hospitalarios por aborto en el �rea de Talcahuano.', '020_01_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(1, '19', '020_01_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(1, 'Salud y medio humano. Contaminaci�n por pesticidas', '020_01_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(1, 'Tecnolog�a M�dica. Diagn�stico de la situaci�n de Tecnolog�a M�dica a nivel universitario y profesional.', '020_01_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(1, 'Inserci�n. Comit� del Memorial Nathalie Masse.', '020_01_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(2, '', '020_02_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(2, '', '020_02_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(2, '', '020_02_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(2, '', '020_02_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(2, 'Formaci�n profesional. La educaci�n y formaci�n del m�dico General.', '020_02_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(2, 'Expectativas de vida. Tablas de mortalidad del momento versus tabla de mortalidad de generaci�n.', '020_02_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(2, 'Ciencias f�sicas y sociales y salud. Modelos energ�ticos y salud.', '020_02_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(2, 'Salud y medio humano. Contaminaci�n marina de Chile.', '020_02_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(2, 'Cardiolog�a. El Congreso de Cardiolog�a de Tokio.', '020_02_09.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(2, 'Inserci�n. Nueva publicaci�n de O. M. S.', '020_02_10.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(3, '', '020_03_01.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(3, '', '020_03_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(3, '', '020_03_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(3, '', '020_03_04.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(3, 'Estructura de morbilidad. Estructura de consultas pedi�tricas en un �rea de salud.', '020_03_05.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(3, 'Crecimiento y desarrollo. Estudio longitudinal del crecimiento del ni�o chileno.', '020_03_06.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(3, 'Salud y Medio Humano. La contaminaci�n del ambiente.', '020_03_07.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(3, 'Peso de nacimiento y desarrollo socioecon�mico. Bajo peso de nacimiento, un gran problema mundial.', '020_03_08.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 20 AND PublishYear = 1979)),
(1, '', '021_01_02.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 21 AND PublishYear = 1979)),
(1, '', '021_01_03.pdf', '', 0, (SELECT MagazineID FROM FinderSchema.Magazine WHERE MagazineNumber = 21 AND PublishYear = 1979));

DECLARE @ID BIGINT;
DECLARE @AID BIGINT;
EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'S.', @AuthorFName = 'Diaz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'H.', @AuthorMName = 'Boccardo', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'A.', @AuthorMName = 'Baeza', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'A.', @AuthorMName = 'Taborga', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'B.', @AuthorMName = 'Viel', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' ', @AuthorFName = 'F.', @AuthorMName = 'Pino', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'Alessandri', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'F. A.', @AuthorFName = 'E.', @AuthorMName = 'Crew', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Robert', @AuthorFName = 'F.', @AuthorMName = 'Schilling', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Brit. Med. Journel', @AuthorFName = '-', @AuthorMName = 'Editorial', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Chester', @AuthorFName = 'S.', @AuthorMName = 'Keefer', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Robert', @AuthorFName = 'A.', @AuthorMName = 'Moore', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'C.', @AuthorFName = 'W.', @AuthorMName = 'Pickering', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'M.', @AuthorMName = 'D.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' F. R.', @AuthorFName = 'C.', @AuthorMName = 'I.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eric', @AuthorFName = 'James', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Milton', @AuthorFName = 'Terris', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Benjam�n', @AuthorFName = 'D.', @AuthorMName = 'Paul', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_01_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'S.', @AuthorFName = 'Diaz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'H.', @AuthorMName = 'Boccardo', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'A.', @AuthorMName = 'Baeza', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' A.', @AuthorFName = 'Taborga', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' ', @AuthorFName = 'B.', @AuthorMName = 'Viel', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'AlfredoSilva', @AuthorFName = 'Santiago', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Henry', @AuthorFName = 'E.', @AuthorMName = 'Sigerist', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'George G.', @AuthorFName = 'Reader', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Mary E.', @AuthorFName = 'W.', @AuthorMName = 'Goos', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'A.', @AuthorFName = 'R.', @AuthorMName = 'Mangus', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Od�n', @AuthorFName = 'W.', @AuthorMName = 'Anderson', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'E.', @AuthorFName = 'A.', @AuthorMName = 'Sch�ller', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Ch.', @AuthorFName = 'R.', @AuthorMName = 'Hoffer', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Ch.', @AuthorFName = 'P.', @AuthorMName = 'Loomis', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' P.', @AuthorFName = 'A.', @AuthorMName = 'Miller.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'A.', @AuthorFName = 'R.', @AuthorMName = 'Magnus', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Joseph', @AuthorFName = 'Ben', @AuthorMName = 'David', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Robert', @AuthorFName = 'K.', @AuthorMName = 'Morton', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Samuel', @AuthorMName = 'Bloom', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Natalie', @AuthorMName = 'Rogoff', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Everett', @AuthorFName = 'C.', @AuthorMName = 'Hughes.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'L.', @AuthorFName = 'D.', @AuthorMName = 'Eron', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'G.', @AuthorFName = 'C.', @AuthorMName = 'Stern', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' J.', @AuthorFName = 'C.', @AuthorMName = 'Scanon', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Benjam�n', @AuthorFName = 'D.', @AuthorMName = 'Paul', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_15.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gabriel', @AuthorFName = 'W.', @AuthorMName = 'Lasker', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_16.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Campbell', @AuthorFName = 'Moses', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_18.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Lucien', @AuthorFName = 'A.', @AuthorMName = 'Gregg', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_18.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' T.', @AuthorFName = 'S.', @AuthorMName = 'Danowski', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_18.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Iago', @AuthorFName = 'Galdstone', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_02_19.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sr. Eduardo', @AuthorFName = 'Hamuy', @AuthorMName = 'B.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_01.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Orlando', @AuthorFName = 'Sep�lveda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'A.', @AuthorFName = 'E.', @AuthorMName = 'Severinghaus', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'F. Grundy y J. M. mackintosh', @AuthorFName = '', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'M.', @AuthorFName = 'R.', @AuthorMName = 'Sand', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'J.', @AuthorFName = 'J.', @AuthorMName = 'Gillon', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hardy', @AuthorFName = 'A.', @AuthorMName = 'Kemp', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'M.', @AuthorMName = 'D.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Iago', @AuthorFName = 'Goldstone', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'F.', @AuthorFName = 'Grundy', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' J.', @AuthorFName = 'M.', @AuthorMName = 'Mackintosh', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'W.', @AuthorFName = 'Mellville', @AuthorMName = 'Arnott', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Thomas', @AuthorFName = 'Mc', @AuthorMName = 'Keown', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' F. A.', @AuthorFName = 'R.', @AuthorMName = 'Stammers', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Ph. D.', @AuthorFName = 'Mc', @AuthorMName = 'Gill', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'W.', @AuthorFName = 'Hobson', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' ', @AuthorFName = 'John', @AuthorMName = 'Pemberton', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Robert', @AuthorFName = 'E.', @AuthorMName = 'Shank', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Paul', @AuthorFName = 'H.', @AuthorMName = 'Kopper', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_15.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Osvaldo', @AuthorFName = 'A.', @AuthorMName = 'Quijada.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_03_16.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'Alessandri', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Francisco', @AuthorFName = 'Hoffmann', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'J. V.', @AuthorFName = 'Santa', @AuthorMName = 'Mar�a', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'H.', @AuthorFName = 'San', @AuthorMName = 'Mart�n', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'J.', @AuthorMName = 'Pe�a', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'W.', @AuthorMName = 'Pizarro', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'colaboradores.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ward', @AuthorFName = 'Darley', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Vannevar', @AuthorFName = 'Bush', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Oswald', @AuthorFName = 'Hall.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'John', @AuthorFName = 'W.', @AuthorMName = 'Stone.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edith', @AuthorFName = 'M.', @AuthorMName = 'Letz', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'R.', @AuthorFName = 'H.', @AuthorMName = 'F�lix', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' J.', @AuthorFName = 'A.', @AuthorMName = 'Clausen.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernest', @AuthorFName = 'Carroll', @AuthorMName = 'Faust', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edgar', @AuthorFName = 'V.', @AuthorMName = 'Allen.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_04_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hugo', @AuthorFName = 'Behm', @AuthorMName = 'R.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sir', @AuthorFName = 'Lionel', @AuthorMName = 'Whithy', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alan', @AuthorFName = 'Valentine', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Raymond', @AuthorFName = 'Whitehead', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Donald', @AuthorFName = 'G.', @AuthorMName = 'Anderson', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'A.', @AuthorFName = 'P.', @AuthorMName = 'Thompson', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sydney', @AuthorFName = 'Sunderland', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'A.', @AuthorFName = 'A.', @AuthorMName = 'Severinghaus', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Dennis', @AuthorFName = 'Brinton', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Frederic', @AuthorFName = 'Bartlett', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'T.', @AuthorFName = 'S.', @AuthorMName = 'Simey', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sir A. L.', @AuthorFName = 'Mudaliar', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Dr. A.', @AuthorFName = 'Morgan', @AuthorMName = 'Jones', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Henry', @AuthorFName = 'Cohen', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'W.', @AuthorFName = 'Melville', @AuthorMName = 'Arnott', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_15.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'J.', @AuthorFName = 'Henry', @AuthorMName = 'Biggart', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_16.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'A.', @AuthorFName = 'Bradford', @AuthorMName = 'Hill', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_17.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Oliver', @AuthorFName = 'Cope.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_18.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'David', @AuthorFName = 'Campbell', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_19.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'A.', @AuthorFName = 'L.', @AuthorMName = 'Richard', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_20.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'A. Hurtad y', @AuthorFName = 'Yan', @AuthorMName = 'Aird', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_21.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Dag', @AuthorFName = 'Knutson', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_22.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Kinloch', @AuthorFName = 'Nelson', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_23.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'G.', @AuthorFName = 'Patric', @AuthorMName = 'Meredith', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_24.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'C.', @AuthorFName = 'V.', @AuthorMName = 'Hackett', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_25.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'R.', @AuthorFName = 'D.', @AuthorMName = 'Lawrence', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_26.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'T.', @AuthorFName = 'R.', @AuthorMName = 'Henn', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_27.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'A.', @AuthorFName = 'Stampar', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_28.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Charles', @AuthorMName = 'Fletcher', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_28.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'F. A.', @AuthorFName = 'E.', @AuthorMName = 'Crew', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_29.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'G.', @AuthorFName = 'W.', @AuthorMName = 'Gale', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_30.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Axel', @AuthorFName = 'Strom', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_31.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'James', @AuthorFName = 'M.', @AuthorMName = 'Faulkner', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_32.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Thomas', @AuthorFName = 'Francis', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_33.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'J.', @AuthorFName = 'P.', @AuthorMName = 'Hubbard', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_05_34.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alejandro', @AuthorFName = 'Garret�n', @AuthorMName = 'Silva', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_06_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ian', @AuthorFName = 'Aird', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_06_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Nicholas', @AuthorFName = 'J.', @AuthorMName = 'Cotsonas', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_06_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Robert', @AuthorFName = 'Kaiser', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_06_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Harry', @AuthorFName = 'Dowling', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_06_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'P.', @AuthorFName = 'Joe', @AuthorMName = 'Baughman', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_06_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'George', @AuthorFName = 'Pickering', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_06_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hugh', @AuthorFName = 'Chaplin', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_06_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Leonard', @AuthorFName = 'Eron', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_06_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'William', @AuthorFName = 'Schofield', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_06_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Arnold', @AuthorFName = 'Starr', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_06_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Grujica', @AuthorFName = 'Zarkovic', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_06_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Urpo', @AuthorFName = 'S�rala', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_06_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jehan', @AuthorFName = 'S.', @AuthorMName = 'Salch', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_06_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gabriela', @AuthorFName = 'Mistral', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'G�mez', @AuthorMName = 'Millas.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'Alessandri', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Amador', @AuthorFName = 'Neghme', @AuthorMName = 'R.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alejandro', @AuthorFName = 'Garret�n', @AuthorMName = 'Silva', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gustavo', @AuthorFName = 'Fricke', @AuthorMName = 'Schencke', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Guillermo', @AuthorFName = 'Valenzuela', @AuthorMName = 'L.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'Urz�a', @AuthorMName = 'M.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Arturo', @AuthorFName = 'baeza', @AuthorMName = 'Go�i', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Francisco', @AuthorFName = 'Hoffman', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'H�ctor', @AuthorFName = 'Orrego', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'J. V.', @AuthorFName = 'Santa', @AuthorMName = 'Mar�a', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_13.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ra�l', @AuthorFName = 'Yazigi', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_14.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rafael', @AuthorFName = 'Darricarrere', @AuthorMName = 'T.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_15.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ignacio', @AuthorFName = 'Gonz�lez', @AuthorMName = 'G.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_16.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'San', @AuthorMName = 'Mart�n', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '001_07_17.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Francisco', @AuthorFName = 'Rojas', @AuthorMName = 'Villegas', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '002_01_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Alfredo', @AuthorFName = 'Leonardo', @AuthorMName = 'Bravo', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '002_01_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Roberto', @AuthorFName = 'Barahona', @AuthorMName = 'Silva', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '002_01_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Rafael', @AuthorFName = 'Darricarrere', @AuthorMName = 'Torbalay', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '002_01_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ana', @AuthorFName = 'Mar�a', @AuthorMName = 'Kaempfer', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '002_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Orlando', @AuthorFName = 'Sep�lveda', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '002_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gustavo', @AuthorFName = 'Molina', @AuthorMName = 'Mart�nez.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '002_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '002_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Frazer', @AuthorFName = 'Brockington', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '002_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Le�n', @AuthorFName = 'Tabah', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '002_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Ra�l', @AuthorMName = 'Samuel', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '002_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Tegualda', @AuthorFName = 'Monreal.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '002_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Luis', @AuthorFName = 'Torres', @AuthorMName = 'Ram�rez', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '002_02_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pablo', @AuthorFName = 'Toledo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '003_01_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Jelic', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '003_01_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Dar�o', @AuthorFName = 'Verdugo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '003_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'Oyanguren', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '003_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Salvador', @AuthorFName = 'D�az', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '003_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Arturo', @AuthorFName = 'baeza', @AuthorMName = 'Go�i', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '003_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Octavio', @AuthorFName = 'Cabello', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '003_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Ricardo', @AuthorMName = 'Cibotti', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '003_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Oscar', @AuthorFName = 'Jim�nez', @AuthorMName = 'Pinochet', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '003_02_02.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Octavio', @AuthorFName = 'Cabello', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '003_02_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Ricardo', @AuthorMName = 'Cibotti', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '003_02_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Rosselot', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '003_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gustavo', @AuthorFName = 'Molina.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '003_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Leonardo', @AuthorMName = 'Bravo', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'San', @AuthorMName = 'Mart�n', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'J.', @AuthorFName = 'C�sar', @AuthorMName = 'Garc�a', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hugo Behm', @AuthorFName = 'R', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'colaboradores', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edward', @AuthorFName = 'S.', @AuthorMName = 'Rogers.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eugenio', @AuthorFName = 'Gonz�lez', @AuthorMName = 'Rojas', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_03_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Amador', @AuthorFName = 'Neghme', @AuthorMName = 'R.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Skewes', @AuthorMName = 'O.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fernando', @AuthorFName = 'Huidobro', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Salvador', @AuthorFName = 'D�az', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_03_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Informe', @AuthorFName = 'de', @AuthorMName = 'Secretar�a', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_03_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Guillermo', @AuthorFName = 'Adriazola', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_03_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Informe Comit� de Expertos de la OMS. Ginebra. 13-19 de Agosto. 1963', @AuthorFName = '', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_03_12.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Benjam�n', @AuthorFName = 'Viel', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_04_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Enrique', @AuthorFName = 'Hurtado', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rafael', @AuthorFName = 'Darricarrere', @AuthorMName = 'T.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jos� M.', @AuthorFName = 'Ugarte', @AuthorMName = 'A.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Julio', @AuthorFName = 'Raffo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '004_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Elliot', @AuthorFName = 'Freidson.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '005_01_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'J. C�sar', @AuthorFName = 'Garc�a', @AuthorMName = 'L.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '005_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlota', @AuthorFName = 'R�os', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '005_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Adela', @AuthorFName = 'Berdichesky', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '005_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carmen', @AuthorFName = 'A.', @AuthorMName = 'Mir�', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '005_02_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gustavo Molina', @AuthorFName = 'G.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '005_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Carlos Montoya', @AuthorFName = 'A.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '005_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Oscar', @AuthorMName = 'Jim�nez.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '005_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jos�', @AuthorFName = 'Vera', @AuthorMName = 'Lamprein', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '005_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'Dur�n', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '005_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = 'Alvarez.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '005_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Albeto', @AuthorFName = 'Britto.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '005_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'Romero', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = 'falta de la 13_16');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Salvador', @AuthorFName = 'D�az.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_01_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'L.', @AuthorFName = 'Weinstein', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ra�l', @AuthorFName = 'Devoto', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Elliot', @AuthorFName = 'Freidson.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gunnar', @AuthorFName = 'Myrdal.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_02_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Riquelme.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Riquelme.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Francisco', @AuthorFName = 'Mardones', @AuthorMName = 'Restat.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_03_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Adela', @AuthorFName = 'Berdichesky.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_03_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Albert', @AuthorFName = 'F.', @AuthorMName = 'Wessen', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gustavo', @AuthorFName = 'Molina.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Norberto Espinosa Solis', @AuthorFName = 'de', @AuthorMName = 'Ovando.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Doris', @AuthorFName = 'Krebs.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'Romero.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_04_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Salvador', @AuthorFName = 'D�az.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_04_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Henry Van', @AuthorFName = 'Zile', @AuthorMName = 'Hyde.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '006_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Riquelme', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Joaqu�n', @AuthorFName = 'Undurraga', @AuthorMName = 'Correa.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Norberto Espinosa Sol�s', @AuthorFName = 'de', @AuthorMName = 'Ovando.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'San', @AuthorMName = 'Mart�n', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = 'A.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_01_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Rosselot', @AuthorMName = 'V.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Onofre', @AuthorFName = 'Avenda�o', @AuthorMName = 'P.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Jos� M.', @AuthorFName = 'Borgo�o', @AuthorMName = 'D.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Oscar', @AuthorFName = 'Soto', @AuthorMName = 'G.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Gloria', @AuthorFName = 'Molina', @AuthorMName = 'M.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Claudio', @AuthorFName = 'Jimeno', @AuthorMName = 'G.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eyzer', @AuthorFName = 'Klorman', @AuthorMName = 'Katz', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Paula Pelaez', @AuthorFName = 'de', @AuthorMName = 'A.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Claudio', @AuthorFName = 'Jimeno', @AuthorMName = 'G.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Gullermo', @AuthorMName = 'Cumsille', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Armando', @AuthorFName = 'Roa.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alberto', @AuthorFName = 'Cristoffanini', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Jorge', @AuthorFName = 'Alvarez', @AuthorMName = 'A.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Hern�n', @AuthorFName = 'Romero', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Hern�n', @AuthorMName = 'Oyanguren.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alejandro', @AuthorFName = 'Jim�nez', @AuthorMName = 'Arango.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Robin F.', @AuthorFName = 'Badgley', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Marjorie', @AuthorMName = 'Schulte.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n San', @AuthorFName = 'Mart�n', @AuthorMName = 'F.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_03_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Jos� Pe�a', @AuthorFName = 'D.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_03_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'F.', @AuthorMName = 'Biel', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_03_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Adela', @AuthorFName = 'Berdichewsky', @AuthorMName = 'G.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Salvador', @AuthorFName = 'D�az', @AuthorMName = 'P.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Carlos', @AuthorMName = 'Montoya', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Gustavo', @AuthorFName = 'Molina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Roberto', @AuthorMName = 'Belmar.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Salvador', @AuthorFName = 'D�az.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Paula Pel�ez', @AuthorFName = 'de', @AuthorMName = 'A.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '007_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gustavo', @AuthorFName = 'Molina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Salvador', @AuthorFName = 'D�az', @AuthorMName = 'R.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Georgina', @AuthorMName = 'Ortiz', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Enrique', @AuthorFName = 'Mu�oz', @AuthorMName = 'Faundez', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'H�ctor', @AuthorFName = 'Aliaga', @AuthorMName = 'G.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jos� M.', @AuthorFName = 'Ugarte', @AuthorMName = 'A.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos Montoya', @AuthorFName = 'A.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Iv�n', @AuthorFName = 'Videla', @AuthorMName = 'V.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Uribe', @AuthorMName = 'Concha', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Hern�n', @AuthorFName = 'San', @AuthorMName = 'Mart�n', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Ilabaca', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Comisi�n Organizadora', @AuthorFName = 'Secretar�a', @AuthorMName = 'Ejecutiva', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_02_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hernan', @AuthorFName = 'Romero', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hugo', @AuthorFName = 'Behm', @AuthorMName = 'R.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Leonel', @AuthorFName = 'Alvarez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Jose', @AuthorFName = 'M.', @AuthorMName = 'Pujol', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Varleta', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Comisi�n Organizadora', @AuthorFName = 'Secretar�a', @AuthorMName = 'Ejecutiva', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_03_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'David', @AuthorFName = 'M.', @AuthorMName = 'Shaw', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_03_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Claude', @AuthorFName = 'R.', @AuthorMName = 'Nichols', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_03_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Morton', @AuthorFName = 'D.', @AuthorMName = 'Bogdonoff', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_03_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edward', @AuthorFName = 'Grzegorzewski', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_03_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Anthony M.', @AuthorFName = 'M.', @AuthorMName = 'Payne', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Lautaro', @AuthorFName = 'Osorio', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Enrique', @AuthorFName = 'Hurtado', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Comisi�n Organizadora', @AuthorFName = 'Secretar�a', @AuthorMName = 'Ejecutiva', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gabriela', @AuthorFName = 'Venturini', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_04_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'Eugenia', @AuthorMName = 'Radrig�n', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_04_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Augusto', @AuthorFName = 'Winter', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '008_04_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'Romero.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Salvador', @AuthorFName = 'D�az', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Arthur', @AuthorFName = 'J.', @AuthorMName = 'Rubel', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Milton', @AuthorFName = 'Terris', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Francisco', @AuthorFName = 'Acevedo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_01_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Adela', @AuthorFName = 'Berdichewsky', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'Urz�a', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Bernard', @AuthorFName = 'E.', @AuthorMName = 'Segal', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Elso Schiappacasse', @AuthorFName = 'F.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_02_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Fructuoso', @AuthorFName = 'Biel', @AuthorMName = 'C.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_02_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Jadresic', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Ignacio', @AuthorMName = 'Monge', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rafael', @AuthorFName = 'T.', @AuthorMName = 'Darricarrere', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jos�', @AuthorFName = 'Ugarte', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Adela', @AuthorFName = 'Berdichewsky', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_03_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Horacio', @AuthorMName = 'Boccardo', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_03_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'colaboradores', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_03_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'Romero', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_03_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fernando', @AuthorFName = 'Monckeberg', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Adela', @AuthorFName = 'Legarreta', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Giorgio', @AuthorMName = 'Solimano', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Herbert', @AuthorFName = 'E.', @AuthorMName = 'Klarman', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Bernard', @AuthorFName = 'E.', @AuthorMName = 'Segal', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'The Journal of', @AuthorFName = 'Medial', @AuthorMName = 'Education', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '009_04_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hernan', @AuthorFName = 'Romero', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Anibal', @AuthorFName = 'Faundez-Latham', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Germ�n', @AuthorFName = 'Rodriguez', @AuthorMName = 'Galant', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Onofre', @AuthorMName = 'Avenda�o', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rub�n', @AuthorFName = 'Puentes', @AuthorMName = 'Rojas', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edward', @AuthorFName = 'A.', @AuthorMName = 'Schuman.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gabriela', @AuthorFName = 'Venturini', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_01_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Salvador', @AuthorMName = 'D�az', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_01_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Cristina', @AuthorMName = 'Palma', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_01_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'M.', @AuthorFName = 'Francoise', @AuthorMName = 'Hall', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Robin', @AuthorFName = 'F.', @AuthorMName = 'Bagdley', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Robert', @AuthorFName = 'W.', @AuthorMName = 'Hetherrington', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' V.', @AuthorFName = 'L.', @AuthorMName = 'Matthews', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Marjorie', @AuthorMName = 'Schulte', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rebeca', @AuthorFName = 'Soto', @AuthorMName = 'Villegas', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' H�ctor', @AuthorFName = 'Rodriguez', @AuthorMName = 'Maturana', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' J. Luis', @AuthorFName = 'Gonz�lez', @AuthorMName = 'Rodr�guez', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Leoncio', @AuthorFName = 'Leiva', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edward A. Schuman.The Milbank Memorial Fund Quarterly', @AuthorFName = '', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_02_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jose', @AuthorFName = 'Quiroga', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_02_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Cristina', @AuthorMName = 'Palma', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_02_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Gabriela', @AuthorMName = 'Venturini', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_02_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Patri', @AuthorMName = 'M', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Giorgio', @AuthorMName = 'Solimano', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Raquel', @AuthorMName = 'Fleishman', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'M.', @AuthorFName = 'Francoise', @AuthorMName = 'Hall', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Salvador', @AuthorFName = 'Diaz.', @AuthorMName = 'P.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Edgardo', @AuthorFName = 'Condeza', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Colegio M�dico', @AuthorFName = 'de', @AuthorMName = 'Chile', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_03_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Secretaria', @AuthorFName = 'Ejecutiva', @AuthorMName = '(Informacion)', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_03_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Iv�n', @AuthorFName = 'Videla', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = 'Lois', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Ana', @AuthorFName = 'Maria', @AuthorMName = 'Kaempffer', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'M.', @AuthorFName = 'Francoise', @AuthorMName = 'Hall', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Isabel', @AuthorMName = 'Allende', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Patricio', @AuthorMName = 'Garc�a', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Marta', @AuthorMName = 'Burgos', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Ana', @AuthorFName = 'Mar�a', @AuthorMName = 'Dagnino', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '010_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rene', @AuthorFName = 'Dubos', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hernan', @AuthorFName = 'Romero.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Richard', @AuthorFName = 'H.', @AuthorMName = 'Barnes', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'Luisa', @AuthorMName = 'tarr�s', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' A.', @AuthorFName = 'M.', @AuthorMName = 'Wiedmaier', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'P.', @AuthorMName = 'Espejo', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'S.', @AuthorMName = 'Faiguenbaum', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rolando', @AuthorFName = 'Merino', @AuthorMName = 'S.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_01_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Wanda', @AuthorFName = 'Pizarro', @AuthorMName = 'V.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_01_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Rodrigo', @AuthorFName = 'Rojas', @AuthorMName = 'Mc.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_01_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fernando', @AuthorFName = 'Monckeberg', @AuthorMName = 'Barros', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hernan', @AuthorFName = 'Romero.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carl', @AuthorFName = 'E.', @AuthorMName = 'Taylor', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Marie-', @AuthorFName = 'Francoise', @AuthorMName = 'Hall', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Gaete', @AuthorMName = 'Avaria', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Isabel', @AuthorFName = 'Tapia', @AuthorMName = 'Porta', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'H.', @AuthorFName = 'Behm', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'colaboradores.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Claudio', @AuthorFName = 'Sepulveda', @AuthorMName = 'A.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hector', @AuthorFName = 'Gutierrez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Monica', @AuthorMName = 'Pr�ger', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jos� M.', @AuthorFName = 'Ugarte', @AuthorMName = 'A.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Enzo', @AuthorFName = 'Devoto', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_03_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Stephen', @AuthorFName = 'J.', @AuthorMName = 'Plank', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_03_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Maria', @AuthorFName = 'Lucila', @AuthorMName = 'Milanesi', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_03_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fidel', @AuthorFName = 'Urrutia.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Laura', @AuthorFName = 'Cornejo', @AuthorMName = 'C.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Ra�l', @AuthorFName = 'Palma', @AuthorMName = 'E.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Salvador', @AuthorFName = 'Diaz', @AuthorMName = 'P', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Hern�n', @AuthorFName = 'Pardo', @AuthorMName = 'P', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Silvia', @AuthorFName = 'Pessoa', @AuthorMName = 'O.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fabian', @AuthorFName = 'Regalde', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Giorgio', @AuthorMName = 'Solimano', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Alfredo', @AuthorMName = 'Avenda�o', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '011_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'An�bal', @AuthorFName = 'Faundez', @AuthorMName = 'L', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' German', @AuthorFName = 'Rodriguez', @AuthorMName = 'G', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Ellen', @AuthorFName = 'Hardy', @AuthorMName = 'E', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Rafael', @AuthorFName = 'Mozo', @AuthorMName = 'A.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Homero', @AuthorFName = 'V�squez', @AuthorMName = 'Mesa', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Claudio R.', @AuthorFName = 'Sepulveda', @AuthorMName = 'A.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Laura', @AuthorMName = 'Cornejo', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Erich', @AuthorMName = 'Nicholls', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Raul', @AuthorMName = 'Palma', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Carlos', @AuthorMName = 'Valenzuela', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ellen', @AuthorFName = 'Hardy', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'G.', @AuthorFName = 'N.', @AuthorMName = 'Serdiukovskaya', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ponencia', @AuthorFName = 'Cubana', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Claudio R', @AuthorFName = 'Sepulveda', @AuthorMName = 'A', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' J. Carlos', @AuthorFName = 'Concha', @AuthorMName = 'G', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Carlos', @AuthorFName = 'Molina', @AuthorMName = 'B', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Francisco', @AuthorFName = 'Rojas', @AuthorMName = 'Ochoa', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ivan', @AuthorFName = 'Videla', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_03_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ramon', @AuthorFName = 'Vilalon', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Maria', @AuthorFName = 'Angelica', @AuthorMName = 'Tagle', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Salvador', @AuthorFName = 'D�az', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hernan', @AuthorFName = 'Romero', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hernan', @AuthorFName = 'Sotomayor', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Reuel', @AuthorFName = 'A.', @AuthorMName = 'Stallones.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_04_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'A.', @AuthorFName = 'Faundez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'E.', @AuthorMName = 'Hardy', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'G.', @AuthorMName = 'Henr�quez', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Agustin', @AuthorFName = 'cruz', @AuthorMName = 'melo', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Carlos', @AuthorFName = 'Montoya', @AuthorMName = 'Aguilar', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Chiorrini', @AuthorMName = 'Alverti', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' enrique', @AuthorFName = 'Espinosa', @AuthorMName = 'Soto', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Lucy', @AuthorFName = 'Novajas', @AuthorMName = 'Urz�a.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = 'Aguilera', @AuthorMName = 'Covarrubias', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '012_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ximena', @AuthorFName = 'Diaz', @AuthorMName = 'Beer.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mario', @AuthorFName = 'Gonz�lez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' equipo', @AuthorFName = 'de', @AuthorMName = 'salud.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan Pablo', @AuthorFName = 'Schifini', @AuthorMName = 'P.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Horacio', @AuthorFName = 'Boccardo', @AuthorMName = 'Z.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Raimundo', @AuthorMName = 'Hederra', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Hilda', @AuthorMName = 'Fierro', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Nora', @AuthorFName = 'Ibarra', @AuthorMName = 'Fernandez.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Guillermo', @AuthorFName = 'Adriazola', @AuthorMName = 'E.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_01_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hilda', @AuthorFName = 'Fierro', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Raimundo', @AuthorMName = 'Hederra', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Horacio', @AuthorMName = 'Boccardo', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Raimundo', @AuthorFName = 'Hederra.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Horacio', @AuthorFName = 'Boccardo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Raimundo', @AuthorMName = 'Hederra.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alex', @AuthorFName = 'Papic.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_02_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfonso', @AuthorFName = 'Gonz�lez.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Bogolav', @AuthorFName = 'Juricic', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Claudio', @AuthorFName = 'Sep�lveda', @AuthorMName = 'A.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Ra�l', @AuthorFName = 'Palma', @AuthorMName = 'E.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Alberto', @AuthorFName = 'Rodr�guez', @AuthorMName = 'S.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Giorgio', @AuthorFName = 'Solimano', @AuthorMName = 'Cantuarias', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Hugo', @AuthorFName = 'Unda', @AuthorMName = 'D�az', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Alfonso', @AuthorFName = 'Alvarez', @AuthorMName = 'Prieto', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Hugo', @AuthorMName = 'Mu�oz', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Silvia', @AuthorMName = 'Morris', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Fernando', @AuthorMName = 'Qui�ones.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Manuel', @AuthorFName = 'Z��iga.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Jungjohann', @AuthorMName = 'Sch.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '013_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hernan', @AuthorFName = 'Montenegro', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Guillermo', @AuthorMName = 'Adriasola', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Gloria', @AuthorFName = 'Jaramillo', @AuthorMName = 'G.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Patricio de la Puente L.', @AuthorFName = '', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Montoya', @AuthorMName = 'A.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Hugo', @AuthorFName = 'Mu�oz', @AuthorMName = 'C.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Silvia', @AuthorFName = 'Morris', @AuthorMName = 'B.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Fernando', @AuthorFName = 'Qui�ones', @AuthorMName = 'L.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'L.', @AuthorMName = 'Minguell', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ivan', @AuthorFName = 'Videla', @AuthorMName = 'V�squez', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Patricio', @AuthorFName = 'Silva', @AuthorMName = 'Gar�n', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mario', @AuthorFName = 'Herrera', @AuthorMName = 'Moore', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'S.', @AuthorFName = 'J.', @AuthorMName = 'Plank', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' M.', @AuthorFName = 'L.', @AuthorMName = 'Milanesi', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfonso', @AuthorFName = 'Calvo', @AuthorMName = 'Belmar', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Alfredo', @AuthorFName = 'Ovalle', @AuthorMName = 'Salas', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Samuel', @AuthorFName = 'Valdivia', @AuthorMName = 'Soto', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'William', @AuthorFName = 'A.', @AuthorMName = 'Reinke', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_03_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Patri', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Hayd�e', @AuthorMName = 'Sep�lveda', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Alfredo', @AuthorMName = 'Avenda�o', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Maria', @AuthorFName = 'Eugenia', @AuthorMName = 'Radrigan', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Mario', @AuthorMName = 'Gonzalez', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Lita', @AuthorMName = 'Cornejo', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Mercedes', @AuthorMName = 'Gonzalez', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Gloria', @AuthorMName = 'Pozo.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Marconi', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Etienne', @AuthorFName = 'Berthet', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Valenzuela', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_04_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Alfredo', @AuthorMName = 'Avenda�o', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_04_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Emilia', @AuthorMName = 'D�az', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_04_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Eulalia', @AuthorMName = 'Wildner', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_04_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Natacha', @AuthorFName = 'N��ez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Leorado', @AuthorMName = 'Villarroel', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Guillermo', @AuthorFName = 'Adriasola', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'M�nica', @AuthorMName = 'Pinto', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Sergio', @AuthorMName = 'Maltes', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Naveillan.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '014_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Raquel', @AuthorFName = 'Saba', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Cecilia', @AuthorMName = 'Rodriguez', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Ana', @AuthorMName = 'Gonz�lez', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Adriana', @AuthorFName = 'Sch�lchli', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ra�l', @AuthorFName = 'Zapata', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carmen', @AuthorFName = 'Gacit�a.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Armando', @AuthorFName = 'Wagner', @AuthorMName = 'Retamal.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Angel', @AuthorFName = 'Morales.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Naveillan.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Osvaldo', @AuthorFName = 'Sotomayor', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Osvaldo', @AuthorFName = 'Sotomayor.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_02_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jose', @AuthorFName = 'Ugarte', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_03_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Hernan', @AuthorMName = 'Urzua.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_03_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Iv�n', @AuthorFName = 'Contreras', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Raul', @AuthorFName = 'Zapata.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Adriana', @AuthorFName = 'Schalchli', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Enrique', @AuthorMName = 'Spichiger', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ilse', @AuthorFName = 'Lopez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_04_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Elisa', @AuthorMName = 'Alvarado', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_04_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Sara', @AuthorMName = 'Arcuch', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_04_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Carmen', @AuthorMName = 'Cabiol', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_04_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Eliana', @AuthorMName = 'Rivera', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_04_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Nestor', @AuthorFName = 'Irribarra', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' equipo', @AuthorFName = 'de', @AuthorMName = 'salud.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Nicolas', @AuthorFName = 'Gonzalez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Armando', @AuthorMName = 'Bardales', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Eduardo', @AuthorMName = 'Hitschfeld', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '015_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Patri', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Libertad', @AuthorMName = 'Figueroa', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Eliana', @AuthorMName = 'Cerda', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Ivan', @AuthorMName = 'Saavedra', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Graciela', @AuthorMName = 'Serra', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Haydee', @AuthorMName = 'Villal�n.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mario', @AuthorFName = 'Garces', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Edmundo', @AuthorMName = 'James', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Hern�n', @AuthorMName = 'Castillo', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Jorge', @AuthorMName = 'Morales', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Carmen', @AuthorFName = 'Gloria', @AuthorMName = 'Salas', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Estela', @AuthorMName = 'Campos', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carlos', @AuthorFName = 'Barria', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' equipo', @AuthorFName = 'de', @AuthorMName = 'salud.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Horacio', @AuthorFName = 'Espoz', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Avenda�o', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_02_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Carlos', @AuthorMName = 'Valenzuela', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_02_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Alfredo', @AuthorMName = 'Patri', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_02_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Maria', @AuthorFName = 'Eliana', @AuthorMName = 'Cerda', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_02_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Libertad', @AuthorMName = 'Figueroa', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_02_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Haydee', @AuthorMName = 'Villal�n', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_02_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Graciela', @AuthorMName = 'Serra', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_02_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Iv�n', @AuthorMName = 'Saavedra', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_02_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Eulalia', @AuthorMName = 'Wildner.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_02_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'Venturino', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Francisco', @AuthorFName = 'Menchaca', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'H�ctor', @AuthorFName = 'Rodr�guez.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jos�', @AuthorFName = 'Ugarte.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Avenda�o', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Carlos', @AuthorMName = 'Valenzuela', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Alfredo', @AuthorMName = 'Patri', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Iv�n', @AuthorMName = 'Saavedra', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Julio', @AuthorFName = 'Basoalto', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Horacio', @AuthorMName = 'Boccardo.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Maria', @AuthorFName = 'Teresa', @AuthorMName = 'Dobert.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jos�', @AuthorFName = 'Ugarte.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Avenda�o', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Carlos', @AuthorMName = 'Valenzuela', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Alfredo', @AuthorMName = 'Patri', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jos�', @AuthorFName = 'Avenda�o', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Rafael', @AuthorFName = 'Enderica', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_04_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Maria', @AuthorFName = 'Teresa', @AuthorMName = 'Dobert.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '016_04_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Delia', @AuthorFName = 'Bravo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Maria', @AuthorFName = 'C.', @AuthorMName = 'Concha', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Hernan', @AuthorFName = 'Venturino', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'otros', @AuthorMName = 'autores.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jos�', @AuthorFName = 'Valenzuela', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Emilia', @AuthorMName = 'Diaz', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'B�rbara', @AuthorMName = 'Klagges', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jos�', @AuthorFName = 'Jim�nez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Alfredo', @AuthorMName = 'Patri.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Julio', @AuthorFName = 'Monreal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Germ�n', @AuthorFName = 'Corey', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Gustavo', @AuthorFName = 'Rencoret', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_01_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'In�s', @AuthorMName = 'Romero', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Nicol�s', @AuthorMName = 'Majluf', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Guacolda', @AuthorMName = 'Ubilla', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Mariano', @AuthorMName = 'Guerrero', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Gonzalo', @AuthorMName = 'Palacios', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fernanda', @AuthorFName = 'Mesquida', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Carmen', @AuthorMName = 'Naveil�n', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Maria', @AuthorFName = 'E.', @AuthorMName = 'Campusano', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Pedro', @AuthorMName = 'Naveill�n', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Paz', @AuthorMName = 'Zulic.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Agust�n', @AuthorFName = 'Gallardo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Juan', @AuthorMName = 'S�nchez', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Delia', @AuthorFName = 'Bravo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Maria', @AuthorFName = 'C.', @AuthorMName = 'Concha', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Hern�n', @AuthorFName = 'Venturino', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'otros', @AuthorMName = 'autores.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Francisco', @AuthorFName = 'J.', @AuthorMName = 'Menchaca', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Salud P�blica y', @AuthorFName = 'Bienestar', @AuthorMName = 'Social.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_02_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ana', @AuthorFName = 'Maria', @AuthorMName = 'Kaempffer', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_03_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Ernesto', @AuthorMName = 'medina', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_03_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Maria de', @AuthorFName = 'la', @AuthorMName = 'Fuente', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_03_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Maria', @AuthorFName = 'In�s', @AuthorMName = 'Romero.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_03_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jos�', @AuthorFName = 'M.', @AuthorMName = 'Ugarte', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_03_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Julio', @AuthorFName = 'Basoalto', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Ana', @AuthorFName = 'M.', @AuthorMName = 'Kaempffer', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'In�s', @AuthorMName = 'Romero', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Nicol�s', @AuthorMName = 'Majluf', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Guacolda', @AuthorMName = 'Ubilla', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Mariano', @AuthorMName = 'Guerrero', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Gonzalo', @AuthorMName = 'Palacios', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_04_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria de', @AuthorFName = 'la', @AuthorMName = 'Fuente', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Julia', @AuthorMName = 'Gonz�lez', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Hugo', @AuthorMName = 'Mu�oz', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Fernando', @AuthorMName = 'Mu�oz', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Gladys', @AuthorMName = 'Yentzen', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mat�as', @AuthorFName = 'Tijmes.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Avenda�o', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Maria', @AuthorFName = 'In�s', @AuthorMName = 'Romero', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Nicol�s', @AuthorMName = 'Majluf', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Guacolda', @AuthorMName = 'Ubilla', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Mariano', @AuthorMName = 'Guerrero', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Gonzalo', @AuthorMName = 'Palacios', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '017_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Naveill�n.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Filomena', @AuthorFName = 'Falaha', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Manuela', @AuthorFName = 'Vicente', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Edith', @AuthorMName = 'Aguilera', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Honold', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'Montenegro', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Eduardo', @AuthorMName = 'Medina', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Naveill�n.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Retamal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Hern�n', @AuthorFName = 'Oyanguren', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_02_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Santiago', @AuthorFName = 'Urcelay', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_02_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Mar�a', @AuthorFName = 'V.', @AuthorMName = 'Thomas', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_02_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Myrtha', @AuthorMName = 'Sims', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_02_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Naveill�n', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carmen', @AuthorFName = 'Steinert', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Ver�nica', @AuthorMName = 'Valdenegro', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Luis', @AuthorMName = 'Espinoza', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Carlos', @AuthorMName = 'Ubilla.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Avenda�o', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Domingo', @AuthorMName = 'As�n', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Alfredo', @AuthorMName = 'Patri', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Patricia', @AuthorMName = 'Hamel', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mar�a de', @AuthorFName = 'la', @AuthorMName = 'Fuente', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Cecilia', @AuthorMName = 'Albala', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ramiro', @AuthorFName = 'Molina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Jorge', @AuthorMName = 'L�pez', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'marcial', @AuthorMName = 'Orellana', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Eduardo', @AuthorMName = 'Orlandi.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Patricio', @AuthorFName = 'Mena.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jos�', @AuthorFName = 'M.', @AuthorMName = 'Ugarte', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sergio', @AuthorFName = '�lvarez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '018_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ramiro', @AuthorFName = 'Molina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Jorge', @AuthorMName = 'L�pez', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Marcial', @AuthorMName = 'Orellana', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Eduardo', @AuthorMName = 'Orlandi', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Julia', @AuthorMName = 'Alarc�n.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'C.', @AuthorFName = 'Naveill�n', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' M.', @AuthorFName = 'E.', @AuthorMName = 'Campusano', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'P.', @AuthorMName = 'Zulic', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'P.', @AuthorMName = 'Naveill�n', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'M.', @AuthorMName = 'Frauenberg', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'M.', @AuthorMName = 'Fruns.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Germ�n', @AuthorFName = 'Guerra.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Bombil', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Lorenzo', @AuthorMName = 'Naranjo', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Elba', @AuthorMName = 'Olivares', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Roberto', @AuthorFName = 'Von', @AuthorMName = 'Dessauer.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_02_03.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carmen', @AuthorFName = 'Velasco', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Jos�', @AuthorFName = 'M.', @AuthorMName = 'Ugarte', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_02_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Daniel', @AuthorFName = 'Juricic', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'C.', @AuthorFName = 'Naveill�n', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' M.', @AuthorFName = 'E.', @AuthorMName = 'Campusano', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'P.', @AuthorMName = 'Zulic', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'P.', @AuthorMName = 'Naveill�n', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'M.', @AuthorMName = 'Frauenberg', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'M.', @AuthorMName = 'Fruns.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Patri.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Guillermo', @AuthorFName = 'Adriasola.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Marcel', @AuthorFName = 'Trucco.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Retamal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_03_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Pedro', @AuthorFName = 'Naveill�n', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_03_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Adela', @AuthorMName = 'Legarreta', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_03_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Silvia', @AuthorMName = 'Pessoa', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_03_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Mario', @AuthorMName = 'Sep�lveda', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_03_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'H�ctor', @AuthorMName = 'S�nchez.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_03_10.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ilse', @AuthorFName = 'L�pez', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_03_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Elcira', @AuthorMName = 'Galdames', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_03_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Sergio', @AuthorMName = 'Vargas', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_03_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Miriam', @AuthorMName = 'Castellanos', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_03_11.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jos�', @AuthorFName = 'Ugarte', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Mirella', @AuthorFName = 'de', @AuthorMName = 'Kartzow', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Juan', @AuthorFName = 'Margozzini', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'colaboradores.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Horacio', @AuthorFName = 'Boccardo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Daniel', @AuthorMName = 'Juricic', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Saleh', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '019_04_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carmen', @AuthorFName = 'Velasco', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Jos�', @AuthorMName = 'Ugarte', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Patricia', @AuthorMName = 'Diaz.', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_04_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ernesto', @AuthorFName = 'Medina', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Ana', @AuthorMName = 'Kaempffer', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_04_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Julio', @AuthorFName = 'Monreal', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_04_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Sylvia', @AuthorFName = 'Asenjo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Elizabeth', @AuthorMName = 'Molina', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Gioconda', @AuthorMName = 'Boggiano', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_04_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Norambuena', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Hugo', @AuthorMName = 'Mu�oz', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_01_04.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Ram�n', @AuthorFName = 'Florenzano', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Oscar', @AuthorMName = 'Feuerhake', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_01_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Agust�n', @AuthorFName = 'Gallardo', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_01_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Fresia', @AuthorFName = 'Solis.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_01_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Solicitada', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_01_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Carmen', @AuthorFName = 'Velasco.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_02_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jorge', @AuthorFName = 'Kamps', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_02_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Eduardo', @AuthorFName = 'Medina.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_02_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Nora', @AuthorFName = 'Cabrera.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_02_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Jaime', @AuthorFName = 'Perez-Olea', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_02_09.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Myriam', @AuthorFName = 'Castellanos', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Ilse', @AuthorMName = 'L�pez', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Juan', @AuthorMName = 'Margozzini', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Nora', @AuthorMName = 'Palacios', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_03_05.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Alfredo', @AuthorFName = 'Patri', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'Haydee', @AuthorMName = 'Sep�lveda', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = ' Carlos', @AuthorFName = 'Valenzuela', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'colaboradores.', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_03_06.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Germ�n', @AuthorFName = 'Corey', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_03_07.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = 'Angele', @AuthorFName = 'Petros-Barvazian', @AuthorMName = '', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END

EXECUTE FinderSchema.cms_CreateAuthor @AuthorName = '', @AuthorFName = 'M.', @AuthorMName = 'Behar', @AuthorID = @ID OUTPUT;
SET @AID = (SELECT ArticleID FROM FinderSchema.Article WHERE ArticleFile = '020_03_08.pdf');
IF NOT EXISTS (SELECT * FROM FinderSchema.ArticleAuthor WHERE ArticleID = @AID)
BEGIN
INSERT INTO FinderSchema.ArticleAuthor (ArticleID, AuthorID) VALUES (@AID, @ID);
END