USE Finder
GO

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'AuthorView')
   DROP VIEW FinderSchema.AuthorView
GO

CREATE VIEW FinderSchema.AuthorView
AS
	SELECT DISTINCT
		AuthorID, AuthorFName, AuthorMName, AuthorName,
		(AuthorFName + ' ' + AuthorMName + AuthorName) AS AuthorFullName
	FROM FinderSchema.Author
GO

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'MagazineView')
   DROP VIEW FinderSchema.MagazineView
GO

CREATE VIEW FinderSchema.MagazineView
AS
	SELECT
		MagazineName, MagazineNumber, PublishYear,
		CAST('Volumen ' + CAST(MagazineNumber AS VARCHAR) + ' del a�o ' + CAST(PublishYear AS VARCHAR) AS VARCHAR(MAX)) AS MagazineTitle,
		CAST(
			CAST(M.MagazineID AS VARCHAR) + ';' +
			CAST(MagazineNumber AS VARCHAR) + ';' +
			CAST(PublishYear AS VARCHAR)
		AS VARCHAR(MAX)) AS MagazineMeta
	FROM FinderSchema.Magazine M
GO

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'ArticleAuthorView')
   DROP VIEW FinderSchema.ArticleAuthorView
GO

CREATE VIEW FinderSchema.ArticleAuthorView
AS
	SELECT AuthorFName, AuthorMName, AuthorName, AA.ArticleID, AA.AuthorID 
	FROM FinderSchema.ArticleAuthor AA
		RIGHT OUTER JOIN FinderSchema.Author ON Author.AuthorID = AA.AuthorID
		LEFT OUTER JOIN FinderSchema.Article ON Article.ArticleID = AA.ArticleID
GO

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'MagazineArticleView')
   DROP VIEW FinderSchema.MagazineArticleView
GO

CREATE VIEW FinderSchema.MagazineArticleView
AS
	SELECT MagazineName, MagazineNumber, PublishYear, Article.*
	FROM FinderSchema.Magazine
		LEFT JOIN FinderSchema.Article ON Article.MagazineID = Magazine.MagazineID
GO

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'ArticleKeywordView')
   DROP VIEW FinderSchema.ArticleKeywordView
GO

CREATE VIEW FinderSchema.ArticleKeywordView
AS
	SELECT KeywordValue, AK.ArticleID, AK.KeywordID
	FROM FinderSchema.ArticleKeyword AK
		RIGHT OUTER JOIN FinderSchema.Article ON Article.ArticleID = AK.ArticleID
		LEFT OUTER JOIN FinderSchema.Keyword ON Keyword.KeywordID = AK.KeywordID
GO

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'ArticleView')
   DROP VIEW FinderSchema.ArticleView
GO

CREATE VIEW FinderSchema.ArticleView
AS
	SELECT Article.*
	FROM FinderSchema.Article
GO

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'PreferenceView')
   DROP VIEW FinderSchema.PreferenceView
GO

CREATE VIEW FinderSchema.PreferenceView
AS
	SELECT * FROM FinderSchema.Preference;
GO

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'MagArtAutView')
	DROP VIEW FinderSchema.MagArtAutView
GO

CREATE VIEW FinderSchema.MagArtAutView
AS
	SELECT DISTINCT
		A.ArticleID, ArticleNumber, ArticleTitle, ArticleFile, ArticleDesc, ArticlePageCount, A.MagazineID,
		MagazineName, MagazineNumber, PublishYear,
		CAST('Volumen ' + CAST(MagazineNumber AS VARCHAR) + ' del a�o ' + CAST(PublishYear AS VARCHAR) AS VARCHAR(MAX)) AS MagazineTitle,
		CAST(
			CAST(M.MagazineID AS VARCHAR) + ';' +
			CAST(MagazineNumber AS VARCHAR) + ';' +
			CAST(PublishYear AS VARCHAR)
		AS VARCHAR(MAX)) AS MagazineMeta,
		STUFF(
		(
			SELECT DISTINCT ',' + CAST(A.AuthorID AS VARCHAR) + ';' + A.AuthorFName + ' ' + A.AuthorMName + ' ' + A.AuthorName
			FROM FinderSchema.ArticleAuthor StuffAA
				INNER JOIN FinderSchema.Author A ON StuffAA.AuthorID = A.AuthorID
			WHERE StuffAA.ArticleID = AA.ArticleID
			FOR XML PATH(''), TYPE
		).value('.', 'varchar(max)'), 1, 1, ''
	) Authors
	FROM FinderSchema.Magazine M
		LEFT JOIN FinderSchema.Article A ON A.MagazineID = M.MagazineID
		LEFT JOIN FinderSchema.ArticleAuthor AA ON AA.ArticleID = A.ArticleID
		LEFT JOIN FinderSchema.Author Aut ON Aut.AuthorID = AA.AuthorID
GO